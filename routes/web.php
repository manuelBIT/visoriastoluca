<?php


Route::get('/','IndexController@index');

Route::get('/register',['as'=>'register','uses'=>'User\RegisterController@index' ]);

Route::post('/registeruser',['as'=>'registeruser','uses'=>'User\RegisterController@store' ]);


Route::post('/availablevisitation',['as'=>'availablevisitation','uses'=>'User\RegisterController@searchvisitation' ]);

Route::post('/availablevisitations',['as'=>'availablevisitations','uses'=>'User\RegisterController@searchvisitationss' ]);

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::post('/findvisitation',['as'=>'findvisitation','uses'=>'User\RegisterController@findvisitation' ]);

Route::post('/availableteam',['as'=>'availableteam','uses'=>'User\RegisterController@visitationteam' ]);

Route::get('/towns/{id}',['as'=>'towns','uses'=>'User\RegisterController@getTowns']);

Route::post('/availableteamoption',['as'=>'availableteamoption','uses'=>'User\RegisterController@visitationteamoption' ]);

Route::post('pdf', 'PdfController@invoice');
$router->get('import', 'ImportController@import');

Route::get('/', function () {
    $posts = App\Post::all();
    return view('/Index/index', compact('posts'));
});

Route::get('/', function () {
    $gallery_images = App\Gallery::all();
    return view('/Index/index', compact('gallery_images'));
});

Route::get('/prueba', ['uses' => 'VisoriaController@index']);

Route::post('pdfresponsiva',['as'=>'pdfresponsiva','uses'=>'PdfController@createdocument']);
Route::get('confirmregister',['as'=>'confirmregister','uses'=>'User\RegisterController@confirm']);
Route::get('import', 'ImportController@import');
Route::post('/availableteamview',['as'=>'availableteamview','uses'=>'User\RegisterController@visitationteamview' ]);