/*!

 =========================================================
 * Bootstrap Wizard - v1.1.1
 =========================================================
 
 * Product Page: https://www.creative-tim.com/product/bootstrap-wizard
 * Copyright 2017 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/bootstrap-wizard/blob/master/LICENSE.md)
 
 =========================================================
 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 */

// Get Shit Done Kit Bootstrap Wizard Functions

searchVisible = 0;
transparent = true;
    /*  Activate the tooltips      */
    $('[rel="tooltip"]').tooltip();
jQuery.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || /^[a-z áéíóúñüàè,' ',]+$/i.test(value);
    }, "Solo letras");

jQuery.validator.addMethod("numberssonly", function(value, element) {
      return this.optional(element) || /^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/i.test(value);
    }, "Solo numeros");
    var $validator = $('#form-register').validate({
          rules: {
            grecaptcharesponse:{
                required:true
            },
            curp_user:{
                required:true
            },
            city: {
              required: true
            },
            towns: {
              required: true
            },
            birthday: {
              required: true
            },
            name:{
                required:true,
                lettersonly:true
            },
            apat_name:{
                required:true,
                lettersonly:true
            },
            amat_name:{
                required:true,
                lettersonly:true
            },
            tutor:{
                required:true,
                lettersonly:true
            },
            birthplace:{
                required:true
            },
            street:{
                required:true
            },
            colony:{
                required:true
            },
            postal_code:{
                required:true
            },
            email:{
                required:true
            },
            telephone:{
                required:true,
                digits: true,
                numberssonly:true
            },
        },
                messages: {
            curp_user:{
                required:"Este campo es requerido."
            },
            towns:{
                required:"Este campo es requerido.",
            },
            name: {
                required: "Este campo es requerido.",
                lettersonly: "Solo letras."
            },
            apat_name: {
                required: "Este campo es requerido.",
                lettersonly: "Solo letras."
            },
            amat_name: {
                required: "Este campo es requerido.",
                lettersonly: "Solo letras."
            },
            address: {
                required : "Este campo es requerido."
            },
            birthday: {
                required: "Este campo es requerido."
            },
            birthplace:{
                required: "Este campo es requerido."
            },
            fathername:{
                required: "Este campo es requerido."
            },
            mothername:{
                required: "Este campo es requerido."
            },
            password:{
                required: "Este campo es requerido"
            },
            confirm_password:{
                required: "Este campo es requerido.",
                equalTo: "Las contraseñas no coinciden"
            },
            username:{
                required: "Este campo es requerido."
            },
            email:{
                required: "Este campo es requerido.",
                email: "Ingrese una dirección de correo valida."
            },
            telephone:{
                required:"Este campo es requerido.",
                digits: "Por favor ingresa solo numeros",
                numberssonly: "Este campo debe contener 10 digitos"
            },
            street: {
                required: "Este campo es requerido."
            },
            colony: {
                required: "Este campo es requerido."
            },
            postal_code: {
                required: "Este campo es requerido."
            },
            city: {
                required: "Este campo es requerido."
            },
            tutor:{
                required: "Este campo es requerido."
            }

        }
    });


   $('.next-step3').click(function() {
        var $valid = $('#form-register').valid();
            if(!$valid) {

                $validator.focusInvalid();
                return false;
            }
            else
            {
                    document.getElementById("step-second").style.display = "none";
                    document.getElementById("step-three").style.display = "block"; 
            }

    });




$('.next-step4').click(function() {
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

    var $valid = $('#form-register').valid();
    var birthday = $("#birthday").val();
    function returnresults() {
        var res;
        $.ajax({
            async:false,
            type:  'post',
            data: {birthday:birthday},
            url:   'http://localhost/visoriastoluca/public/findvisitation',
            success: function(data){
                res = data;
            },
            error:function (xhr, ajaxOptions, thrownError){
            }
                         });
             return res;
    };

    var response = grecaptcha.getResponse();
        if(!$valid) { 
            $validator.focusInvalid();
            return false;
        }
        else {
                if (response.length == 0){
                    $('#recaptcha-error').html("<span style='color:red'>Favor de validar el captcha</span>");
                    return false;
                }
                else{
                    var estadonac = document.getElementById("city");
                    var selected = estadonac.options[estadonac.selectedIndex].text;
                    if (selected=="Aguascalientes")
                    {
                        selected = "AS";
                    }
                    if (selected=="Baja California")
                    {
                        selected = "BC";
                    }
                    if (selected=="Baja California Sur")
                    {
                        selected = "BS";
                    }
                    if (selected=="Campeche")
                    {
                        selected = "CC";
                    }
                    if (selected=="Coahuila de Zaragoza")
                    {
                        selected = "CL";
                    }
                    if (selected=="Colima")
                    {
                        selected = "CM";
                    }
                    if (selected=="Chiapas") 
                    {
                        selected = "CS";
                    }
                    if (selected=="Chihuahua") 
                    {
                        selected = "CH";
                    }
                    if (selected=="Ciudad de Mexico") 
                    {
                        selected = "DF";
                    }
                    if (selected=="Durango") 
                    {
                        selected = "DG";
                    }
                    if (selected=="Guanajuato") 
                    {
                        selected = "GT";
                    }
                    if (selected=="Guerrero") 
                    {
                        selected = "GR";
                    }
                    if (selected=="Guerrero") 
                    {
                        selected = "GR";
                    }
                    if (selected=="Hidalgo") 
                    {
                        selected = "HG";
                    }
                    if (selected=="Jalisco") 
                    {
                        selected = "JC";
                    }
                    if (selected=="México") 
                    {
                        selected = "MC";
                    }
                    if (selected=="Michoacán de Ocampo") 
                    {
                        selected = "MN";
                    }
                    if (selected=="Morelos") 
                    {
                        selected = "MS";
                    }
                    if (selected=="Nayarit") 
                    {
                        selected = "NT";
                    }
                    var formcurp = $("#curp_user").val();
                    var nombreforcurp = $("#name").val();
                    var apatforcurp = $("#apat_name").val();
                    var amatforcurp = $("#amat_name").val();
                    var sexoforcurp = $('input:radio[name=sex]:checked').val();
                    var fechanacimiento = $("#birthday").val();
                    var fechaforcurp = fechanacimiento.split("-");
                    var dia = fechaforcurp[0];
                    var mes = fechaforcurp[1];
                    var year = fechaforcurp[2];
                        var curp = generaCurp({
                        nombre            : ""+nombreforcurp+"",
                        apellido_paterno  : ""+apatforcurp+"",
                        apellido_materno  : ""+amatforcurp+"",
                        sexo              : ""+sexoforcurp+"",
                        estado            : ""+selected+"",
                        fecha_nacimiento  : [dia, mes, year]
                    });

                    if (formcurp != curp)
                    {
                        $('#recaptcha-error').html("");
                        $("#alert-error").html("La curp no corresponde con los datos ingresados.");
                    }
                    else{
                        var results = returnresults();
                        if (results==1){
                            $('#recaptcha-error').html("");
                            $("#alert-error").html("No hay visorias disponibles para tu categoria.");
                        }
                        else{
                            document.getElementById("content-body").style.display = "block"; 
                            document.getElementById("content-body2").style.display = "block"; 
                            document.getElementById("step-three").style.display = "none"; 
                            document.getElementById("image").style.display = "none";
                            document.getElementById("image").setAttribute('style', 'display:none !important');
                            document.getElementById("form1").style.display = "none";
                            document.getElementById("step-fourth").style.display = "block";
                            document.getElementById("calendar").style.display = "block";
                            // document.getElementById("body-field").style.display = "block"; 
                            document.getElementById("botton-hidden").removeAttribute('style', 'display:none !important'); 
                            document.getElementById("botton-hidden").style.display = "none";  
                            $.ajax({
                                data:{birthday:birthday},
                                url:'http://localhost/visoriastoluca/public/availablevisitation',
                                type:'post',
                                beforeSend:function(){
                                    $("#calendar").html("espere...");
                                },
                                success: function(response){
                                    $("#calendar").html(response);
                                },
                                error:function (xhr, ajaxOptions, thrownError){
                                    alert(xhr.responseText);
                                }
                            });
                        }
                    }
                }
            }
});



    $('.next-stepfinish').click(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var $valid = $('#form-register').valid();
        if(!$valid) {
            $validator.focusInvalid();
            return false;
        }
            else
                    {
                        var email = $("#email").val();
                        var telephone = $("#telephone").val();
                        var street = $("#street").val();
                        var city = $("#city").val();
                        var citytext = $("#city option:selected").text();
                        var town = $("#towns").val();
                        var curp_user = $("#curp_user").val();
                        var colony = $("#colony").val();
                        var postal_code = $("#postal_code").val();
                        var name = $("#name").val();
                        var apat_name = $("#apat_name").val();
                        var amat_name = $("#amat_name").val();
                        var tutor = $("#tutor").val();
                        var birthday = $("#birthday").val();
                        var birthplace = $("#birthplace").val();
                        var weight = $("#weight").val();
                        var height = $("#height").val();
                        var profile = $('input:radio[name=profile_selected]:checked').val();
                        var profile2 = $('input:radio[name=profile_selectedd]:checked').val();
                            if(profile2)
                                {
                                    var profile= $('input:radio[name=profile_selectedd]:checked').val();
                                }
                                    const img1 = $(".img-1").hasClass('selectplayer');
                                    const img2 = $('.img-2').hasClass('selectplayer');
                                    const img3 = $('.img-3').hasClass('selectplayer');
                                    const img4 = $('.img-4').hasClass('selectplayer');
                                    const img5 = $('.img-5').hasClass('selectplayer');
                                    const img6 = $('.img-6').hasClass('selectplayer');
                                    const img7 = $('.img-7').hasClass('selectplayer');
                                    const img8 = $('.img-8').hasClass('selectplayer');
                                    const img9 = $('.img-9').hasClass('selectplayer');
                                    const img10 = $('.img-10').hasClass('selectplayer');
                                    const img11 = $('.img-11').hasClass('selectplayer');
                                        if (img1)
                            {
            var position = $(".img-1").attr('value');
        }
        if (img2)
        {
            var position = $(".img-2").attr('value');
        }
        if (img3)
        {
            var position = $(".img-3").attr('value');
        }
        if (img4)
        {
            var position = $(".img-4").attr('value');
        }
        if (img5)
        {
            var position = $(".img-5").attr('value');
        }
        if (img6)
        {
            var position = $(".img-6").attr('value');
        }
        if (img7)
        {
            var position = $(".img-7").attr('value');
        }
        if (img8)
        {
            var position = $(".img-8").attr('value');
        }
        if (img9)
        {
            var position = $(".img-9").attr('value');
        }
        if (img10)
        {
            var position = $(".img-10").attr('value');
        }
        if (img11)
        {
            var position = $(".img-11").attr('value');
        }
        $.ajax({
            data:{curp_user:curp_user,town:town,city:city,email:email,telephone:telephone,street:street,colony:colony,postal_code:postal_code,name:name,apat_name:apat_name,amat_name:amat_name,tutor:tutor,birthday:birthday,birthplace:birthplace,weight:weight,height:height,position:position,profile:profile},
            url:   'http://localhost/visoriastoluca/public/registeruser',
            type:  'post',
            beforeSend:function(){
            },
            success: function(response){
                if (response==1)
                {
                            $.ajax({
                    data:{curp_user:curp_user,email:email,name:name,apat_name:apat_name,amat_name:amat_name},
            url:   'http://localhost/visoriastoluca/public/pdf',
            type:  'post',
            beforeSend:function(){
            },
            success: function(response){
                
                if(response==1)
                {
                    
                    document.getElementById("image").style.display = "none";
                    document.getElementById("image").setAttribute('style', 'display:none !important');  
                    document.getElementById("form1").style.display = "none";
                    document.getElementById("register-success").style.display = "block";
                    $.ajax({
                             data:{profile:profile,postal_code:postal_code,citytext:citytext,colony:colony,street:street,height:height,position:position,weight:weight, tutor:tutor,telephone:telephone,birthplace:birthplace,birthday:birthday,curp_user:curp_user,email:email,name:name,apat_name:apat_name,amat_name:amat_name},
                            url:'http://localhost/visoriastoluca/public/confirmregister',
                            type:  'get',
                            beforeSend:function(){
                            },
                             success: function(response){
                
                            $("#content-register").html(response);
                
            },
            error:function (xhr, ajaxOptions, thrownError){
               
            }
            });
                }
                
            },
            error:function (xhr, ajaxOptions, thrownError){
              
            }
            });
                }
                if (response==0)
                {
                    $("#error-email").html("Ya existe un usuario registrado con el correo proporcionado.");
                }
            },
            error:function (xhr, ajaxOptions, thrownError){
               
            }
            });
            }
      
    });