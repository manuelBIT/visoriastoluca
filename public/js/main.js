
var options = {
  disableDefaultUI: true,
  zoom: 13,
  center: new google.maps.LatLng(19.2444727, -99.6117767)
}
var map = new google.maps.Map(document.getElementById('map'), options);

var marker = new google.maps.Marker({
  position: new google.maps.LatLng(19.2444727, -99.6117767),
  
  map: map
});

// var infowindow = new google.maps.InfoWindow({
//   content: 'This is an info popup'
// });

// google.maps.event.addListener(marker, 'click', function() {
//   infowindow.open(map, marker);
// });

// infowindow.open(map, marker);