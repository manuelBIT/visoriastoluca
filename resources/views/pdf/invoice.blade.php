<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Anuencia y Responsiva Toluca FC</title>
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
  </head>
  <style type="text/css">
    footer { 
        position: fixed; bottom: -60px;
        left: 0px; 
        right: 0px; 
        background-color: #fff; 
        height: 50px; 
        font-size: 11px;
      }
  </style>
  <body>
        <div class="col-md-12" style="margin-left: 26%;display: inline-block;margin-top: 15px">
          <div class="col-md-6" style="margin-top:-10px;height:105px;display: inline-block;border-right: 2px solid #d0112b;padding: 0px;">
            <img src="images/icons/icon.png" style="margin-right:10px;margin-top: 10px;margin-bottom: -10px" width="82px" height="105px">
          </div>
          <div class="col-md-6" style="color:#d0112b;display: inline-block;padding-bottom:60px;font-size: 25px">
            <b>TOLUCA FC</b>

          </div>
        </div>
    <main>
      <div id="title" style="text-align: center">
          <span style="font-size: 12px"><b>ANUENCIA Y RESPONSIVA DEL JUGADOR</b></span><br><br>
      </div>
<div id="content" style="font-size: 11.7px">
  <p style="text-align: justify;text-indent: 1.1cm">
  Por medio de la presente quien suscribe________________________________________  autorizo para que el jugador<b> {{ $apat }} {{ $amat }} {{ $name }}</b> pueda participar en los eventos, entrenamientos,
  partidos oficiales y amistosos coordinados por el Deportivo Toluca Futbol Club S.A. de C.V., dentro o fuera de sus instalaciones.
  </p>
  <p style="text-align: justify;">
  Por tal motivo, deslindo de toda responsabilidad civil, penal, laboral, mercantil y de cualquier tipo al Deportivo Toluca Futbol Club S.A. de C.V., incluyendo a sus representantes, directivos, empleados, colaboradores, jugadores y filiales.
  </p>
  <p style="text-align: justify;">
    Al respecto, acepto el resultado de la prueba, considerando que la práctica del futbol implica una actividad fisica vigorosa, la cual podría ocasionar lesiones, heridas e incluso la muerte, mismas que no deben ser atendidas por el Deportivo Toluca Futbol Club S.A. de C.V., quedando libre de toda responsabilidad.
    </p>
    <p style="text-align: justify;">
      Queda entendido que no se realizará ningún pago o contraprestación durante el proceso de prueba, tampoco habrá obligación por parte del Deportivo Toluca Futbol Club S.A. de C.V. para contratar posteriormente al jugador.
    </p>
    <p style="text-align: justify;">
      Asimismo, declaro bajo protesta que me encuentro apto para realizar actividad deportiva de alto rendimiento.
    </p>
    <p style="margin-bottom: 20px">
      Toluca, México, a <?php echo date('d')?> de <?php echo date('m')?> de <?php echo date('Y')?>
    </p>
    <br>
    <p style="text-align: center">
      <span style="margin-left:180px;margin-right:180px;position:absolute;width:100%;border-top: 1px solid #ddd">Nombre y Firma</span>
      <br>
      <span style="text-align: center">(Adjuntar identificación oficial)</span>
    </p> 
<table class="table" style="font-size: 9px;margin:0px 0px 0px 0px;">
  <tr><td style="border-right: 2px solid #d0112b;border-bottom: 2px solid #d0112b;border-top: ;border-top: none">NOMBRE</td>
      <td colspan="9" style="border-bottom: 2px solid #d0112b;;border-top: none">{{ $apat }} {{ $amat }} {{ $name }}</td>
  </tr>
  <tr><td style="border-right: 2px solid #d0112b;border-bottom: 2px solid #d0112b">FECHA DE NACIMIENTO</td>
      <td colspan="9" style="border-bottom: 2px solid #d0112b">{{ $birthday }}</td>
  </tr>
  <tr><td style="border-right: 2px solid #d0112b;border-bottom: 2px solid #d0112b">LUGAR DE NACIMIENTO</td>
    <td colspan="9" style="border-bottom: 2px solid #d0112b">{{ $birthplace }} </td>
  </tr>
  <tr><td style="border-right: 2px solid #d0112b;border-bottom: 2px solid #d0112b">DOMICILIO</td>
      <td style="font-size: 8px;border-bottom: 2px solid #d0112b">CALLE</td>
      <td style="font-size: 9px;border-bottom: 2px solid #d0112b">{{ $street }}</td>
      <td style="font-size: 8px;border-bottom: 2px solid #d0112b">COL.</td>
      <td style="font-size: 9px;border-bottom: 2px solid #d0112b">{{ $colony }} </td>
      <td style="font-size: 8px;border-bottom: 2px solid #d0112b">C.P.</td>
      <td style="font-size: 9px;border-bottom: 2px solid #d0112b">{{ $postalcode }}</td>
      <td style="font-size: 8px;border-bottom: 2px solid #d0112b">CIUDAD</td>
      <td style="font-size: 9px;border-bottom: 2px solid #d0112b" colspan="2"></td>
  </tr>
  <tr><td style="border-right: 2px solid #d0112b;border-bottom: 2px solid #d0112b">TELÉFONO</td>
    <td colspan="9" style="border-bottom: 2px solid #d0112b">{{ $telephone }}</td>
  </tr>
  <tr><td style="border-right: 2px solid #d0112b;border-bottom: 2px solid #d0112b">NOMBRE DEL PADRE O TUTOR</td>
    <td colspan="9" style="border-bottom: 2px solid #d0112b">{{ $tutor }}</td>
  </tr>
  <tr><td style="border-right: 2px solid #d0112b;border-bottom: 2px solid #d0112b">CORREO ELECTRÓNICO</td>
    <td colspan="9" style="border-bottom: 2px solid #d0112b">{{ $email }}</td>
  </tr>
  <tr><td style="border-right: 2px solid #d0112b;border-bottom: 2px solid #d0112b">ENVIADO POR</td>
    <td colspan="9" style="border-bottom: 2px solid #d0112b"></td>
  </tr>
  <tr><td style="border-right: 2px solid #d0112b;border-bottom: 2px solid #d0112b">POSICIÓN</td>
    <td colspan="3" style="border-bottom: 2px solid #d0112b"> {{ $position }}</td>
    <td colspan="2" style="border-bottom: 2px solid #d0112b">PERFIL</td><td colspan="4" style="border-bottom: 2px solid #d0112b">{{ $profile }}</td>
  </tr>
  <tr><td style="border-right: 2px solid #d0112b;border-bottom: 2px solid #d0112b">PESO</td>
    <td colspan="3" style="border-bottom: 2px solid #d0112b">{{ $weight }}kg</td><td colspan="2" style="border-bottom: 2px solid #d0112b">ESTATURA</td><td colspan="4" style="border-bottom: 2px solid #d0112b">{{ $height }}mts</td>
  </tr>
  <tr><td style="font-size:8px;border-right: 2px solid #d0112b;border-bottom: 2px solid #d0112b">PADECE ALGUNA ENFERMEDAD</td>
    <td colspan="3" style="border-bottom: 2px solid #d0112b"></td>
     <td colspan="2" style="border-bottom: 2px solid #d0112b">FECHA DE VISORÍA</td><td colspan="4" style="border-bottom: 2px solid #d0112b"></td>
  </tr>
</table>
<p>
<center><span style="font-size: 8px">EL CLUB NO SE HACE RESPONSABLE DE LAS LESIONES, TRAUMATISMOS Y SECUELAS, QUE EL SOLICITANTE PUEDA SUFRIR DURANTE SU PROCESO.</span>
<br></center>
<center><span style="font-size: 9px">He leído y acepto los términos antes mencionados.</span></center>
</p>
</div>
<footer>
  <div style="float:left;border-top: 1px solid #000;padding-left:15px;padding-right: 15px">
    Firma de aceptación del solicitante
  </div>
  <div style="float:right;border-top: 1px solid #000;padding-left:20px;padding-right: 20px">
    Firma del padre o tutor
  </div>
</footer>
  </body>
</html>