<div class="col-md-12"  >
  <div class="col-md-12" style="text-align: center;margin-top: 0px;">
  	<img src="images/logo-toluca.png" width="100px" height="100px"><br>
      <span style="font-size:36px;text-align: center;width: 100%;color: #3d464d;opacity: 1">Registro exitoso</span>
      <p style="font-size: 17px;margin-top: 20px;">
        Gracias por registrarse, en breve recibirá un correo de confirmación.
      </p>
      <p style="margin-top: 125px;">
   
      	<form action="{{ route('pdfresponsiva') }}" method="POST">
      		    {{ csrf_field() }}
      		<input type="hidden" name="email" value="{{ $email }}">
			<input type="hidden" name="name" value="{{ $name }}">
			<input type="hidden" name="apat_name" value="{{ $apat }}">
			<input type="hidden" name="amat_name" value="{{ $amat }}">
			<input type="hidden" name="birthday" value="{{ $birthday }}">
			<input type="hidden" name="position" value="{{ $position }}">
			<input type="hidden" name="weight" value="{{ $weight }}">
			<input type="hidden" name="tutor" value="{{ $tutor }}">
			<input type="hidden" name="telephone" value="{{ $telephone }}">
			<input type="hidden" name="birthplace" value="{{ $birthplace }}">
			<input type="hidden" name="height" value="{{ $height }}">
			<input type="hidden" name="street" value="{{ $street }}">
			<input type="hidden" name="colony" value="{{ $colony }}">
			<input type="hidden" name="citytext" value="{{ $citytext }}">
			<input type="hidden" name="postalcode" value="{{ $postal_code }}">
			<input type="hidden" name="profile" value="{{ $profile }}">
			<input type="submit" name="enviar" id="enviarform" value="Descarga tu responsiva">
      	</form>

      </p>
  </div>
</div>
