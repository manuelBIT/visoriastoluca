<div class="row">
    <div class="form-group">
        {{-- <p>{{ $states }}</p> --}}
      <label>Estado</label>

      <select name="city" id="city" class="form-control">
        <option value="">Selecciona una opción</option>
        @foreach ($states as $state)
          <option value="{{ $state->id }}">{{ $state->name }}</option>
        @endforeach
      </select>
    </div>
    <div class="form-group">
      <label>Municipio</label>
      <select name="towns" id="towns" class="form-control">
          <option value="">Selecciona una opción</option>
      </select>
    </div>
        

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Unidad:</strong>
            {!! Form::text('unidad', null, array('placeholder' => 'Unidad','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Categoría:</strong>
            {!! Form::textarea('categoria', null, array('placeholder' => 'Categoria','class' => 'form-control','style'=>'height:150px')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>

