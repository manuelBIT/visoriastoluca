@extends('voyager::master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            
            <h1 class="page-title">
            	
		        <i class="icon voyager-documentation"></i>
		        Agregar nueva visoria
		    </h1>
            <div class="pull-right">
                <a class="btn btn-primary" href="/admin/visoria"> Volver</a>
            </div>
        </div>
    </div>
    @if (count($errors) < 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Hubo algunos problemas con tus datos.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="/visoria/store" method="post">
    	<div class="col-md-12">
    		<div class="panel panel-bordered">
    			@include('Visoria.form')
    		</div>

    	</div>

    		

    </form>

    {{-- {!! Form::open(array('route' => 'store','method'=>'POST')) !!}
         @include('Visorias.form')
    {!! Form::close() !!} --}}


@endsection

