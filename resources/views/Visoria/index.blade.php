@extends('voyager::master')



@section('css')
    <style>
        .panel .mce-panel {
            border-left-color: #fff;
            border-right-color: #fff;
        }

        .panel .mce-toolbar,
        .panel .mce-statusbar {
            padding-left: 20px;
        }

        .panel .mce-edit-area,
        .panel .mce-edit-area iframe,
        .panel .mce-edit-area iframe html {
            padding: 0 10px;
            min-height: 350px;
        }

        .mce-content-body {
            color: #555;
            font-size: 14px;
        }

        .panel.is-fullscreen .mce-statusbar {
            position: absolute;
            bottom: 0;
            width: 100%;
            z-index: 200000;
        }

        .panel.is-fullscreen .mce-tinymce {
            height:100%;
        }

        .panel.is-fullscreen .mce-edit-area,
        .panel.is-fullscreen .mce-edit-area iframe,
        .panel.is-fullscreen .mce-edit-area iframe html {
            height: 100%;
            position: absolute;
            width: 99%;
            overflow-y: scroll;
            overflow-x: hidden;
            min-height: 100%;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-documentation"></i>
        Visorias
    </h1>

    <a href="/admin/visoria/create" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>Añadir nuevo</span>
    </a>
    {{-- @include('voyager::multilingual.language-selector') --}}

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Title</th>
            <th>Body</th>
            <th width="280px">Action</th>
        </tr>
    @foreach ($Visorias as $visoria)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $visoria->title}}</td>
        <td>{{ $visoria->body}}</td>
        <td>
            <a class="btn btn-info" href="{{ route('visorias.show',$visoria->id) }}">Show</a>
            <a class="btn btn-primary" href="{{ route('visorias.edit',$visoria->id) }}">Edit</a>
            {!! Form::open(['method' => 'DELETE','route' => ['visorias.destroy', $visoria->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>

@stop
	


@section('javascript')
    <script>
        $('document').ready(function () {
            $('#slug').slugify();

       
    </script>
@stop