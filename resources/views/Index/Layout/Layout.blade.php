<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<!--<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Roboto:300,400,500,700" rel="stylesheet" type="text/css" />-->
	<link rel="shortcut icon" href="{{{ asset('images/favicon-tolucafc.png') }}}">
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" href="css/main.css" type="text/css" />
	<!-- One Page Module Specific Stylesheet -->
	<link rel="stylesheet" href="css/onepage.css" type="text/css" />
	<!-- / -->

	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons/et/et-line.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" href="css/fonts.css" type="text/css" />

	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />


	<!-- Document Title
	============================================= -->
	<title>TolucaFC - Visorias</title>

</head>

<body class="stretched" data-loader="11" data-loader-color="#543456">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="full-header transparent-header border-full-header static-sticky dark" data-sticky-class="not-dark" data-sticky-offset="full" data-sticky-offset-negative="200">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="index.html" class="standard-logo" data-dark-logo="images/logotol.png"><img src="images/logotol.png" alt="Canvas Logo"></a>
						{{-- <a href="index.html" class="retina-logo" data-dark-logo="images/canvasone-dark@2x.png"><img src="images/canvasone@2x.png" alt="Canvas Logo"></a> --}}
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu">

						<ul class="one-page-menu" data-easing="easeInOutExpo" data-speed="1250" data-offset="65">
							<li><a href="#" data-href="#wrapper"><div>Inicio</div></a></li>
							<li><a href="#" data-href="#section-about"><div>Acerca de</div></a></li>
							<li><a href="#" data-href="#section-instructor"><div>Instructores</div></a></li>
							<li><a href="#" data-href="#section-galery"><div>Galeria</div></a></li>
							<li><a href="#" data-href="#section-contact"><div>Contacto</div></a></li>
						</ul>

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->

		<!-- Slider
		============================================= -->
		<section id="slider" class="slider-parallax full-screen">

			<div class="slider-parallax-inner">

				<div class="full-screen section nopadding nomargin noborder ohidden" style="background-image: url('images/visorias-club.png'); background-size: cover; background-position: top center;">


					<div class="row nomargin" style="position: relative; z-index: 2;">
						<div class="slider-caption slider-caption-center">
							
							<div class="center" style="margin-top: 200px!important;"><a href="/register" class="button button-border button-light button-rounded button-large noleftmargin nobottommargin fadeInUp animated">Inscríbete aquí</a></div>
								{{-- <div class="heading-block nobottomborder bottommargin-sm">
									<h1 style="font-size: 22px;">Iniciar sesión</h1>
									<span style="font-size: 16px;" class="t300 capitalize ls1 notopmargin">Get Started within 5 Minutes.</span>
								</div>
								<form action="#" class="clearfix" style="max-width: 300px;">
									<div class="col_full">
										<label for="" class="capitalize t600">Correo electrónico</label>
										<input type="email" id="template-op-form-email" name="template-op-form-email" value="" class="form-control" />
									</div>
									<div class="col_full">
										<label for="" class="capitalize t600">Contraseña</label>
										<input type="password" id="template-op-form-password" name="template-op-form-password" value="" class="form-control" />
									</div>
									<div class="col_full nobottommargin">
										<button type="submit" class="t400 capitalize button button-border button-dark button-circle nomargin" value="submit">Entrar</button>
									</div>
								</form> --}}
	
						</div>
					</div>

					<a href="#" data-scrollto="#section-about" data-easing="easeInOutExpo" data-speed="1250" data-offset="65" class="one-page-arrow dark"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

				</div>

			</div>

		</section><!-- #slider end -->

		<!-- Content
		============================================= -->
@yield('contenido')

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark noborder">

			<div class="container center">
				<div class="footer-widgets-wrap">

					<div class="row divcenter clearfix">

						<div class="col-md-4">

							<div class="widget clearfix">
								<h4>Links del sitio</h4>

								<ul class="list-unstyled footer-site-links nobottommargin">
									<li><a href="/register" data-scrollto="#wrapper" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Registrarme</a></li>
									<li><a href="#" data-scrollto="#section-about" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Acerca de</a></li>
									<li><a href="#" data-scrollto="#section-calendar" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Calendario</a></li>
									<li><a href="#" data-scrollto="#section-instructor" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Instructores</a></li>
									<li><a href="#" data-scrollto="#section-galery" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Galeria</a></li>
									<li><a href="#" data-scrollto="#section-contact" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Contacto</a></li>
									<li><a href="/admin" data-scrollto="/admin" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Admin</a></li>
								</ul>
							</div>

						</div>

						<div class="col-md-4">

							<div class="widget subscribe-widget clearfix" data-loader="button">
								<img src="images/logotol.png">
							</div>

						</div>

						<div class="col-md-4">

							<div class="widget clearfix">
								{{-- <h4>Contacto</h4> --}}

								<p class="lead">Calle Besana S/N, Col. Barrio del Espiritu Santo,
										52140 Metepec, Méx.</p>

								<div class="center topmargin-sm">
									<a href="https://www.facebook.com/tolucafc" class="social-icon inline-block noborder si-small si-facebook" title="Facebook">
										<i class="icon-facebook"></i>
										<i class="icon-facebook"></i>
									</a>
									<a href="https://twitter.com/TolucaFC" class="social-icon inline-block noborder si-small si-twitter" title="Twitter">
										<i class="icon-twitter"></i>
										<i class="icon-twitter"></i>
									</a>
									{{-- <a href="#" class="social-icon inline-block noborder si-small si-github" title="Github">
										<i class="icon-github"></i>
										<i class="icon-github"></i>
									</a>
									<a href="#" class="social-icon inline-block noborder si-small si-pinterest" title="Pinterest">
										<i class="icon-pinterest"></i>
										<i class="icon-pinterest"></i>
									</a> --}}
								</div>
							</div>

						</div>

					</div>

				</div>
			</div>

			<div id="copyrights">
				<div class="container center clearfix">
					Powered by
					<a href="http://www.bittech.mx/">
						<img style="height: 40px; width: 60px;" src="images/bitAzul3.png">
					</a>
				</div>
			</div>

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/plugins.js"></script>

	
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDv4-ugMfOmU7EsYycza_oK8jD-S7XNnzs"></script>
	{{-- <script type="text/javascript" src="js/jquery.gmap.js"></script> --}}

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>



	<script type="text/javascript" src="js/jquery.gmap.js" >

		// jQuery(window).load(function(){

		// 	// Google Map
		// 	jQuery('#headquarters-map').gMap({
		// 		address: 'Melbourne, Australia',
		// 		maptype: 'ROADMAP',
		// 		zoom: 14,
		// 		markers: [
		// 			{
		// 				address: "Melbourne, Australia",
		// 				html: "Melbourne, Australia",
		// 				icon: {
		// 					image: "images/icons/map-icon-red.png",
		// 					iconsize: [32, 32],
		// 					iconanchor: [14,44]
		// 				}
		// 			}
		// 		],
		// 		doubleclickzoom: false,
		// 		controls: {
		// 			panControl: false,
		// 			zoomControl: false,
		// 			mapTypeControl: false,
		// 			scaleControl: false,
		// 			streetViewControl: false,
		// 			overviewMapControl: false
		// 		},
		// 		styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.country","elementType":"labels.text","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":"-100"},{"lightness":"30"}]},{"featureType":"administrative.neighborhood","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"simplified"},{"gamma":"0.00"},{"lightness":"74"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"lightness":"3"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
		// 	});

		// });

	</script>
	<script type="text/javascript" src="js/main.js"></script>

</body>
</html>