	@extends('Index.Layout.Layout')
	@section('contenido')	
		<section id="content">
					<div class="section nomargin" id="section-about" style="background-color: #303030 !important;">
						<div class="container clearfix">
							<div class="divcenter center" style="max-width: 1500px;">
								<h2 style="color: white !important;" class="nobottommargin t300 ls1">El Club Deportivo Toluca tiene como objetivo ampliar las oportunidades para que lo jóvenes puedan demostrar y desarrollar su talento  a lo largo de toda la República Mexicana, de esta forma contribuimos al desarrollo del deporte en México ampliando la captación de nuevas promesas.</h2>
							</div>
						</div>
					</div>

					<div class="clearfix nopadding">
						<div class="col-md-6">
							<div class="fluid-width-video-wrapper" style="padding-top: 65.25%;">
								<iframe src="http://player.vimeo.com/video/232918826" frameborder="0" allowfullscreen="" id="fitvid1"></iframe>
							</div>
						</div>
						
						{{-- <div class="col-md-6 hidden-xs" style="background: url('images/main-bg.jpg') center center no-repeat; background-size: cover;"></div> --}}
						<div class="col-md-6 nopadding">
							<div class="max-height">
								<div class="row common-height grid-border clearfix">
									<div class="col-md-4 col-sm-6 col-padding">
										<div class="feature-box fbox-center fbox-dark fbox-plain fbox-small nobottomborder">
											<div class="fbox-icon">
												<img src="images/icons/visorias-gratis-01.svg" >
												
											</div>
											<h3>Visorías completamente gratuitas</h3>
										</div>
									</div>
									<div class="col-md-4 col-sm-6 col-padding">
										<div class="feature-box fbox-center fbox-dark fbox-plain fbox-small nobottomborder">
											<div class="fbox-icon">
												<img src="images/icons/registro-online-01.svg" >
											</div>
											<h3>Registro en línea</h3>
										</div>
									</div>
									<div class="col-md-4 col-sm-6 col-padding">
										<div class="feature-box fbox-center fbox-dark fbox-plain fbox-small nobottomborder">
											<div class="fbox-icon">
												<img src="images/icons/posicion-01.svg" >
											</div>
											<h3>Tú eliges tu posición</h3>
										</div>
									</div>
									<div class="col-md-4 col-sm-6 col-padding">
										<div class="feature-box fbox-center fbox-dark fbox-plain fbox-small nobottomborder">
											<div class="fbox-icon">
												<img src="images/icons/tiempo-juego.svg" >
											</div>
											<h3>Tiempo de juego garantizado</h3>
										</div>
									</div>
									<div class="col-md-4 col-sm-6 col-padding">
										<div class="feature-box fbox-center fbox-dark fbox-plain fbox-small nobottomborder">
											<div class="fbox-icon">
												<img src="images/icons/visorias-permanentes-01.svg" >
											</div>
											<h3>Visorías permanentes en metepec</h3>
										</div>
									</div>
									<div class="col-md-4 col-sm-6 col-padding">
										<div class="feature-box fbox-center fbox-dark fbox-plain fbox-small nobottomborder">
											<div class="fbox-icon">
												<img src="images/icons/presencia-nacional-01.svg" >
											</div>
											<h3>Presencia a nivel nacional y EUA</h3>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
			<div class="content-wrap nopadding">

				<div class="section notopborder nomargin" id="section-calendar">

					<div class="container clearfix">	

						<div class="divcenter center" style="max-width: 900px;">
								<h2 class="nobottommargin heading-block">Próximas Visorias</h2>
						</div>										
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="row nomargin">							
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center;margin-top:15px;font-size: 22px;color: #1c2f40;border-bottom: solid 1px #1c2f40">
										<b>OCTUBRE</b>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:15px;">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding" style="font-size: 18px; color: #d0112b;margin-bottom:25px;">
											<b>JIQUIPILCO | 
												<span style="color:#6b6b6b;">EDO.MÉXICO</span>
											</b>
										</div>
										<i class="fa fa-map-marker" style="padding-right: 5px; font-size:18px;">
											Unidad Deportiva
										</i>
										<br>
										<br>
										<i class="fa fa-calendar" style="padding-right: 5px; font-size:18px;">
											13 de Octubre
										</i>
										<br>
										<br>
										<i class="fa fa-clock-o" style="padding-right: 5px; font-size:18px;">
											- 12:00 hrs Categorías 2003-2007 
		                  
										</i>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:15px;">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding" style="font-size: 18px; color: #d0112b;margin-bottom:25px;">
											<b>JIQUIPILCO | 
												<span style="color:#6b6b6b;">EDO.MÉXICO</span>
											</b>
										</div>
										<i class="fa fa-map-marker" style="padding-right: 5px; font-size:18px;">
											Unidad Deportiva
										</i>
										<br>
										<br>
										<i class="fa fa-calendar" style="padding-right: 5px; font-size:18px;">
											13 de Octubre
										</i>
										<br>
										<br>
										<i class="fa fa-clock-o" style="padding-right: 5px; font-size:18px;">
											- 12:00 hrs Categorías 2003-2007 
		                  
										</i>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="row nomargin">							
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center;margin-top:15px;font-size: 22px;color: #1c2f40;border-bottom: solid 1px #1c2f40">
										<b>OCTUBRE</b>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:15px;">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding" style="font-size: 18px; color: #d0112b;margin-bottom:25px;">
											<b>JIQUIPILCO | 
												<span style="color:#6b6b6b;">EDO.MÉXICO</span>
											</b>
										</div>
										<i class="fa fa-map-marker" style="padding-right: 5px; font-size:18px;">
											Unidad Deportiva
										</i>
										<br>
										<br>
										<i class="fa fa-calendar" style="padding-right: 5px; font-size:18px;">
											13 de Octubre
										</i>
										<br>
										<br>
										<i class="fa fa-clock-o" style="padding-right: 5px; font-size:18px;">
											- 12:00 hrs Categorías 2003-2007 
		                  
										</i>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:15px;">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding" style="font-size: 18px; color: #d0112b;margin-bottom:25px;">
											<b>JIQUIPILCO | 
												<span style="color:#6b6b6b;">EDO.MÉXICO</span>
											</b>
										</div>
										<i class="fa fa-map-marker" style="padding-right: 5px; font-size:18px;">
											Unidad Deportiva
										</i>
										<br>
										<br>
										<i class="fa fa-calendar" style="padding-right: 5px; font-size:18px;">
											13 de Octubre
										</i>
										<br>
										<br>
										<i class="fa fa-clock-o" style="padding-right: 5px; font-size:18px;">
											- 12:00 hrs Categorías 2003-2007 
		                  
										</i>
									</div>
								</div>
							</div>
						</div>				
					</div>
				</div>

				<div id="section-instructor" class="center page-section">

					<div class=" clearfix">

						<h2 class="divcenter bottommargin font-body" style="max-width: 700px; font-size: 40px;">Nuestros Instructores</h2>

						{{-- <p class="lead divcenter bottommargin" style="max-width: 800px;">Ford Foundation reduce child mortality fight against oppression refugee disruption pursue these aspirations effect. Free-speech Nelson Mandela change liberal; challenges of our times sustainability institutions.</p> --}}

						{{-- <p class="bottommargin" style="font-size: 16px;"><a href="#" data-scrollto="#section-services" data-easing="easeInOutExpo" data-speed="1250" data-offset="70" class="more-link">Learn more <i class="icon-angle-right"></i></a></p> --}}

						<div class="clear"></div>

						<div class="row topmargin-lg divcenter" style="max-width: 2500px;">

							<div class="col-lg-3 col-sm-3 bottommargin">

								<div class="team">
									<div class="team-image">
										<img src="images/1_01.png" alt="John Doe">
										<div class="team-overlay">
											{{-- <div class="team-social-icons">
												<a href="#" class="social-icon si-borderless si-small si-facebook" title="Facebook">
													<i class="icon-facebook"></i>
													<i class="icon-facebook"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-small si-twitter" title="Twitter">
													<i class="icon-twitter"></i>
													<i class="icon-twitter"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-small si-github" title="Github">
													<i class="icon-github"></i>
													<i class="icon-github"></i>
												</a>
											</div> --}}
										</div>
									</div>
									<div class="team-desc team-desc-bg">
										<div class="team-title"><h4>Aldo Rivas Lopez</h4><span>Coordinador</span></div>
									</div>
								</div>

							</div>

							<div class="col-lg-3 col-sm-3 bottommargin">

								<div class="team">
									<div class="team-image">
										<img src="images/3_01.png" alt="Josh Clark">
										<div class="team-overlay">
											{{-- <div class="team-social-icons">
												<a href="#" class="social-icon si-borderless si-small si-twitter" title="Twitter">
													<i class="icon-twitter"></i>
													<i class="icon-twitter"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-small si-linkedin" title="LinkedIn">
													<i class="icon-linkedin"></i>
													<i class="icon-linkedin"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-small si-flickr" title="Flickr">
													<i class="icon-flickr"></i>
													<i class="icon-flickr"></i>
												</a>
											</div> --}}
										</div>
									</div>
									<div class="team-desc team-desc-bg">
										<div class="team-title"><h4>Moises Figueroa Aguilera</h4><span>Visor</span></div>
									</div>
								</div>

							</div>

							<div class="col-lg-3 col-sm-3 bottommargin">

								<div class="team">
									<div class="team-image">
										<img src="images/2_01.png" alt="Mary Jane">
										<div class="team-overlay">
											{{-- <div class="team-social-icons">
												<a href="#" class="social-icon si-borderless si-small si-twitter" title="Twitter">
													<i class="icon-twitter"></i>
													<i class="icon-twitter"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-small si-vimeo" title="Vimeo">
													<i class="icon-vimeo"></i>
													<i class="icon-vimeo"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-small si-instagram" title="Instagram">
													<i class="icon-instagram"></i>
													<i class="icon-instagram"></i>
												</a>
											</div> --}}
										</div>
									</div>
									<div class="team-desc team-desc-bg">
										<div class="team-title"><h4>Marco Antonio Ferreira Espinosa</h4><span>Visor</span></div>
									</div>
								</div>

							</div>

							<div class="col-lg-3 col-sm-3 bottommargin">

								<div class="team">
									<div class="team-image">
										<img src="images/4_01.png" alt="Mary Jane">
										{{-- <div class="team-overlay">
											<div class="team-social-icons">
												<a href="#" class="social-icon si-borderless si-small si-twitter" title="Twitter">
													<i class="icon-twitter"></i>
													<i class="icon-twitter"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-small si-vimeo" title="Vimeo">
													<i class="icon-vimeo"></i>
													<i class="icon-vimeo"></i>
												</a>
												<a href="#" class="social-icon si-borderless si-small si-instagram" title="Instagram">
													<i class="icon-instagram"></i>
													<i class="icon-instagram"></i>
												</a>
											</div>
										</div> --}}
									</div>
									<div class="team-desc team-desc-bg">
										<div class="team-title"><h4>Agustín Contreras Díaz</h4><span>Visor</span></div>
									</div>
								</div>

							</div>

						</div>

					</div>

				</div>

				<div id="section-galery" class="page-section notoppadding nobottompadding">

					<div class="section nomargin">
						<div class="container clearfix">
							<div class="divcenter center" style="max-width: 900px;">
								<h2 class="nobottommargin heading-block">Galería</h2>
							</div>
						</div>
					</div>

					<!-- Portfolio Items
					============================================= -->
					<div id="portfolio" class="portfolio grid-container portfolio-nomargin portfolio-full portfolio-masonry mixed-masonry clearfix">


						{{-- @foreach($gallery_images as $gallery_image)
								<div class="col-md-3">
									<a href="/gallery/{{ $gallery_image->slug }}">
										<img src="{{ Voyager::image( $gallery_image->image ) }}" style="width:100%">
										<span>{{ $gallery_image->title }}</span>
									</a>
								</div>
						@endforeach --}}
						
						
						@if (empty($gallery_images))
							<div class="divcenter center" style="max-width: 900px;">
								<h3 class="nobottommargin">No hay fotos actualmente</h2>
							</div>
						@else
							@foreach($gallery_images as $gallery_image)
								@if ($gallery_image->featured == 1)
									<article class="portfolio-item pf-media pf-icons wide">
										<div class="portfolio-image">
											<a href="#">
												<img src="{{ Voyager::image( $gallery_image->image ) }}" style="width:100%">
											</a>
											<div class="portfolio-overlay">
												<div class="portfolio-desc">
													<h3><a href="#">{{ $gallery_image->title }}</a></h3>
													{{-- <span><a href="#">Media</a>, <a href="#">Icons</a></span> --}}
												</div>
											</div>
										</div>
									</article>
								@endif
							@endforeach
						@endif

						{{-- <article class="portfolio-item pf-illustrations">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/31.jpg" alt="Locked Steel Gate">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
										<h3><a href="#">Locked Steel Gate</a></h3>
										<span><a href="#">Illustrations</a></span>
									</div>
								</div>
							</div>
						</article>

						<article class="portfolio-item pf-graphics pf-uielements">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/61.jpg" alt="Mac Sunglasses">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
										<h3><a href="#">Mac Sunglasses</a></h3>
										<span><a href="#">Graphics</a>, <a href="#">UI Elements</a></span>
									</div>
								</div>
							</div>
						</article>

						<article class="portfolio-item pf-media pf-icons wide">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/25.jpg" alt="Open Imagination">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
										<h3><a href="#">Open Imagination</a></h3>
										<span><a href="#">Media</a>, <a href="#">Icons</a></span>
									</div>
								</div>
							</div>
						</article>

						<article class="portfolio-item pf-uielements pf-media wide">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/41.jpg" alt="Console Activity">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
										<h3><a href="#">Console Activity</a></h3>
										<span><a href="#">UI Elements</a>, <a href="#">Media</a></span>
									</div>
								</div>
							</div>
						</article>

						<article class="portfolio-item pf-media pf-icons">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/7.jpg" alt="Open Imagination">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
										<h3><a href="#">Open Imagination</a></h3>
										<span><a href="#">Media</a>, <a href="#">Icons</a></span>
									</div>
								</div>
							</div>
						</article>

						<article class="portfolio-item pf-uielements pf-icons">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/5.jpg" alt="Backpack Contents">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
										<h3><a href="#">Backpack Contents</a></h3>
										<span><a href="#">UI Elements</a>, <a href="#">Icons</a></span>
									</div>
								</div>
							</div>
						</article> --}}

					</div><!-- #portfolio end -->

					{{-- <div class="topmargin center"><a href="#" class="button button-border button-circle t600">View More Projects</a></div> --}}

				</div>

				<!--<div id="section-services" class="page-section notoppadding">


					<div class="section dark nomargin">
						<div class="divcenter center" style="max-width: 900px;">
							<h2 class="nobottommargin t300 ls1">Like Our Services? Get an <a href="#" data-scrollto="#template-contactform" data-offset="140" data-easing="easeInOutExpo" data-speed="1250" class="button button-border button-circle button-light button-large notopmargin nobottommargin" style="position: relative; top: -3px;">Instant Quote</a></h2>
						</div>
					</div>

					<div class="section parallax nomargin dark" style="background-image: url('images/page/testimonials.jpg'); padding: 150px 0;" data-stellar-background-ratio="0.3">

						<div class="container clearfix">

							<div class="col_two_fifth nobottommargin">&nbsp;</div>

							<div class="col_three_fifth nobottommargin col_last">

								<div class="fslider testimonial testimonial-full nobgcolor noborder noshadow nopadding" data-arrows="false">
									<div class="flexslider">
										<div class="slider-wrap">
											<div class="slide">
												<div class="testi-content">
													<p>Similique fugit repellendus expedita excepturi iure perferendis provident quia eaque vero numquam?</p>
													<div class="testi-meta">
														Steve Jobs
														<span>Apple Inc.</span>
													</div>
												</div>
											</div>
											<div class="slide">
												<div class="testi-content">
													<p>Natus voluptatum enim quod necessitatibus quis expedita harum provident eos obcaecati id culpa corporis molestias.</p>
													<div class="testi-meta">
														Collis Ta'eed
														<span>Envato Inc.</span>
													</div>
												</div>
											</div>
											<div class="slide">
												<div class="testi-content">
													<p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!</p>
													<div class="testi-meta">
														John Doe
														<span>XYZ Inc.</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>

					</div>

				</div>-->

				<!--<div id="section-blog" class="page-section">

					<h2 class="center uppercase t300 ls3 font-body">Recently From the Blog</h2>

					<div class="section nobottommargin">
						<div class="container clearfix">

							<div class="row topmargin clearfix">

								<div class="ipost col-sm-6 bottommargin clearfix">
									<div class="row">
										<div class="col-md-6">
											<div class="entry-image nobottommargin">
												<a href="#"><img src="images/blog/1.jpg" alt="Paris"></a>
											</div>
										</div>
										<div class="col-md-6" style="margin-top: 20px;">
											<span class="before-heading" style="font-style: normal;">Press &amp; Media</span>
											<div class="entry-title">
												<h3 class="t400" style="font-size: 22px;"><a href="#">Global Meetup Program is Launching!</a></h3>
											</div>
											<div class="entry-content">
												<a href="#" class="more-link">Read more <i class="icon-angle-right"></i></a>
											</div>
										</div>
									</div>
								</div>

								<div class="ipost col-sm-6 bottommargin clearfix">
									<div class="row">
										<div class="col-md-6">
											<div class="entry-image nobottommargin">
												<a href="#"><img src="images/blog/2.jpg" alt="Paris"></a>
											</div>
										</div>
										<div class="col-md-6" style="margin-top: 20px;">
											<span class="before-heading" style="font-style: normal;">Inside Scoops</span>
											<div class="entry-title">
												<h3 class="t400" style="font-size: 22px;"><a href="#">The New YouTube Economy unfolds itself</a></h3>
											</div>
											<div class="entry-content">
												<a href="#" class="more-link">Read more <i class="icon-angle-right"></i></a>
											</div>
										</div>
									</div>
								</div>

								<div class="ipost col-sm-6 bottommargin clearfix">
									<div class="row">
										<div class="col-md-6">
											<div class="entry-image nobottommargin">
												<a href="#"><img src="images/blog/3.jpg" alt="Paris"></a>
											</div>
										</div>
										<div class="col-md-6" style="margin-top: 20px;">
											<span class="before-heading" style="font-style: normal;">Video Blog</span>
											<div class="entry-title">
												<h3 class="t400" style="font-size: 22px;"><a href="#">Kicking Off Design Party in Style</a></h3>
											</div>
											<div class="entry-content">
												<a href="#" class="more-link">Read more <i class="icon-angle-right"></i></a>
											</div>
										</div>
									</div>
								</div>

								<div class="ipost col-sm-6 bottommargin clearfix">
									<div class="row">
										<div class="col-md-6">
											<div class="entry-image nobottommargin">
												<a href="#"><img src="images/blog/4.jpg" alt="Paris"></a>
											</div>
										</div>
										<div class="col-md-6" style="margin-top: 20px;">
											<span class="before-heading" style="font-style: normal;">Inspiration</span>
											<div class="entry-title">
												<h3 class="t400" style="font-size: 22px;"><a href="#">Top Ten Signs You're a Designer</a></h3>
											</div>
											<div class="entry-content">
												<a href="#" class="more-link">Read more <i class="icon-angle-right"></i></a>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>

					<div class="container topmargin-lg clearfix">

						<div id="oc-clients" class="owl-carousel topmargin image-carousel carousel-widget" data-margin="80" data-loop="true" data-nav="false" data-autoplay="5000" data-pagi="false"data-items-xxs="2" data-items-xs="3" data-items-sm="4" data-items-md="5" data-items-lg="6">

							<div class="oc-item"><a href="#"><img src="../images/clients/1.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/2.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/3.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/4.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/5.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/6.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/7.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/8.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/9.png" alt="Clients"></a></div>
							<div class="oc-item"><a href="#"><img src="../images/clients/10.png" alt="Clients"></a></div>

						</div>

					</div>

				</div>-->

				<div id="section-contact" class="page-section notoppadding nobottommargin nobottompadding">

					<div class="section nomargin">
						<div class="container clearfix">
							<div class="divcenter center" style="max-width: 900px;">
								<h2 class="nobottommargin heading-block">Nuestras oficinas</h2>
							</div>
						</div>
					</div>
					<div class="row noleftmargin norightmargin nobottommargin common-height">
						<div  class="col-md-8 col-sm-6 gmap hidden-xs">
							<iframe
							  width="600"
							  height="450"
							  frameborder="0" style="border:0"
							  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBGVmuec_46Bptxmd0Gd3tMU0HcSFuM6AI
							    &q=red+de+escuelas+toluca" allowfullscreen>
							</iframe>
							
						</div>
						<div class="col-md-4 col-sm-6" style="background-color: #e60000;">
							<div class="col-padding max-height" style="color: white !important;" >
								{{-- <h3 style="color: white !important;" class="font-body t400 ls1">Nuestras oficinas</h3> --}}

								<div style="font-size: 16px; line-height: 1.7;">
									<address style="line-height: 1.7;">
										<strong>Metepec:</strong><br>
										Calle Besana S/N, Col. Barrio del Espiritu Santo,<br>
										52140 Metepec, Méx.<br>
									</address>

									<div class="clear topmargin-sm"></div>

									{{-- <address style="line-height: 1.7;">
										<strong>Oficinas del club:</strong><br>
										Constituyentes 6 Pte. #1000, Barrio de San<br>
										Bernardino, 50080 Toluca de Lerdo, Méx.<br>
									</address> --}}

									<div class="clear topmargin"></div>

									<abbr title="Phone Number"><strong>Teléfono: </strong></abbr>(722) 235 0354<br>
									{{-- <abbr title="Fax"><strong>Fax:</strong></abbr> (91) 11 4752 1433<br> --}}
									{{-- <abbr title="Email Address"><strong>Email:</strong></abbr> info@canvas.com --}}
								</div>
							</div>
						</div>
					</div>



					{{-- <div class="container clearfix">

						<div class="divcenter topmargin" style="max-width: 850px;">

							<div class="contact-widget">

								<div class="contact-form-result"></div>

								<form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">

									<div class="form-process"></div>

									<div class="col_half">
										<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control border-form-control required" placeholder="Name" />
									</div>
									<div class="col_half col_last">
										<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control border-form-control" placeholder="Email Address" />
									</div>

									<div class="clear"></div>

									<div class="col_one_third">
										<input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control border-form-control" placeholder="Phone" />
									</div>

									<div class="col_two_third col_last">
										<input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control border-form-control" placeholder="Subject" />
									</div>

									<div class="clear"></div>

									<div class="col_full">
										<textarea class="required sm-form-control border-form-control" id="template-contactform-message" name="template-contactform-message" rows="7" cols="30" placeholder="Your Message"></textarea>
									</div>

									<div class="col_full center">
										<button class="button button-border button-circle t500 noleftmargin topmargin-sm" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Enviar</button>
										<br>
										{{-- <small style="display: block; font-size: 13px; margin-top: 15px;">We'll do our best to get back to you within 6-8 working hours.</small> --}}
									</div>

									<div class="clear"></div>

									<div class="col_full hidden">
										<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
									</div>

								</form>

							</div>

						</div>

					</div>

				</div>

			</div>

		</section>
@stop