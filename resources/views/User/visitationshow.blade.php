<html style="height: 100%">
	<head>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	</head>	
<body style="height: 100%">
<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
<ul class="list-group">
<?php
    $cont = 1;
?>
@if (isset($visitation_finds))
@foreach($visitation_finds as $visitations)
<?php
 $month = date("n", strtotime($visitations->fecha)); 
 $day  = date("d", strtotime($visitations->fecha)); 
 $year  = date("Y", strtotime($visitations->fecha)); 

$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$completdate = $day. " de ".$meses[$month-1]. ", ". $year;

?>
    <li class="list-group-item">
    <input name="date-selected" type="radio" id="<?php echo 'radioo_'.$cont ?>" class="with-gap radio-col-red" value="{{ $visitations->id}}" />
    <label for="<?php echo 'radioo_'.$cont ?>"><b>{{ $visitations->municipio }} {{ $visitations->estado }}</b><br>{{ $visitations->Unidad }} <br><?php echo $completdate ?> - {{ $visitations->hora }} hrs.</label>
    </li>
<?php
    $cont= $cont + 1;
?>
@endforeach
@endif




@if (isset($visitation_find))
@foreach($visitation_find as $visitation)
<?php
 $month = date("n", strtotime($visitation->fecha)); 
 $day  = date("d", strtotime($visitation->fecha)); 
 $year  = date("Y", strtotime($visitation->fecha)); 

$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$completdate = $day. " de ".$meses[$month-1]. ", ". $year;

?>
    
     <li class="list-group-item">
    <input name="date-select" type="radio" id="<?php echo 'radio_'.$cont ?>" class="with-gap radio-col-red radioselect radio-one mobile-select" value="{{ $visitation->idv}}"/>
    <label for="<?php echo 'radio_'.$cont ?>"><b>{{ $visitation->municipio }} , {{ $visitation->estado }}</b><br>{{ $visitation->unidad }} <br><?php echo $completdate ?> - {{ $visitation->hora }} hrs.</label>
    </li>  	
<?php
    $cont= $cont + 1;
?>
@endforeach
@endif
</ul>
</div> 
<div class="col-md-12">
</div>
</div>
<script src="js/bootstrap.min.js"></script>	

</body>
</html>
<script type="text/javascript">
    $('.radio-one').click(function() {

    	var visitation_selected = $('.radio-one:checked').val();
if (visitation_selected)
{
  $.ajaxSetup({
    headers: {  
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
              $.ajax({
        data:{visitation_selected:visitation_selected},
        url:   'http://localhost/visoriastoluca/public/availableteam',
        type:  'post',
        beforeSend:function(){
                 
            $(".field-updated").html("<i class='fa fa-circle-o-notch fa-spin' style='font-size:24px;margin-top:256px;margin-left:264px'></i>");
        },
        success: function(response){
				document.getElementById("field-updated").innerHTML = "";
                $(".field-updated").html(response);
            },
            error:function (xhr, ajaxOptions, thrownError){
              console.log(xhr.responseText);
            }
            });
          }
          else
          {
          	var visitation_selected = $('.radioselect:checked').val();
   $.ajaxSetup({
    headers: {  
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
              $.ajax({
        data:{visitation_selected:visitation_selected},
        url:   'http://localhost/visoriastoluca/public/availableteamview',
        type:  'post',
        beforeSend:function(){
                 
            $("#calendar").html("<i class='fa fa-circle-o-notch fa-spin' style='font-size:24px;margin-top:256px;margin-left:264px'></i>");
        },
        success: function(response){
				document.getElementById("calendar").innerHTML = "";
                $("#calendar").html(response);
            },
            error:function (xhr, ajaxOptions, thrownError){
              console.log(xhr.responseText);
            }
            });
          	
          }
    });
</script>

<script type="text/javascript">
	 //document.getElementById("radio_1").attr.display = "checked"; 
var visitation_selected = $('.radio-one:checked').val();
  $.ajaxSetup({
    headers: {  
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
              $.ajax({
        data:{visitation_selected:visitation_selected},
        url:   'http://localhost/visoriastoluca/public/availableteam',
        type:  'post',
        beforeSend:function(){
                 
            $(".field-updated").html("<i class='fa fa-circle-o-notch fa-spin' style='font-size:24px;margin-top:256px;margin-left:264px'></i>");
        },
        success: function(response){
				document.getElementById("field-updated").innerHTML = "";
                $(".field-updated").html(response);
            },
            error:function (xhr, ajaxOptions, thrownError){
              console.log(xhr.responseText);
            }
            });
</script>

<script type="text/javascript">
    $('.mobile-select').click(function() {

    	var visitation_selected = $('.mobile-select:checked').val();
if (visitation_selected)
{
          	var visitation_selected = $('.mobile-select:checked').val();
   $.ajaxSetup({
    headers: {  
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
              $.ajax({
        data:{visitation_selected:visitation_selected},
        url:   'http://localhost/visoriastoluca/public/availableteamview',
        type:  'post',
        beforeSend:function(){
                 
            $("#calendar").html("<i class='fa fa-circle-o-notch fa-spin' style='font-size:24px;margin-top:256px;margin-left:264px'></i>");
        },
        success: function(response){
				document.getElementById("calendar").innerHTML = "";
                $("#calendar").html(response);
            },
            error:function (xhr, ajaxOptions, thrownError){
              console.log(xhr.responseText);
            }
            });
          }
          else
          {
          	
          }
    });
</script>
<script type="text/javascript">
	
   var alto=$(window).height();
   var ancho=$(window).width();
   if (ancho>767)
   {
   	$(".radioselect").removeClass("mobile-select");
   }
</script>
<script type="text/javascript">
	
document.getElementById('radio_1').setAttribute('checked','checked');
</script>