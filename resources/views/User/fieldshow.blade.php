
<button type="button" class=" btn danger-boton next-step2" id="danger-boton">Continuar</button>
@if (isset($result_playersoption))

@foreach($result_playersoption as $fieldshows)

@if ($fieldshows->descripcion=='Portero')
	@if ($fieldshows->registrados < $fieldshows->total)
				<script type="text/javascript">
				$('.img-1').removeClass('selectplayer');
				$('.img-1').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-1').removeClass('available');
				$('.img-1').addClass('unavailable');
				$('.img-1').css('cursor','default');
				$(".img-1").attr('data-content', 'Agotado');
				</script>
	@endif

@elseif ($fieldshows->descripcion=='Delantero centro')
	@if ($fieldshows->registrados < $fieldshows->total)
				<script type="text/javascript">
				$('.img-6').removeClass('selectplayer');
				$('.img-6').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-6').removeClass('available');
				$('.img-6').addClass('unavailable');
				$('.img-6').css('cursor','default');
				$(".img-6").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif ($fieldshows->descripcion=='Lateral Volante Izquierdo')
	@if ($fieldshows->registrados < $fieldshows->total)
				<script type="text/javascript">
				$('.img-11').removeClass('selectplayer');
				$('.img-11').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-11').removeClass('available');
				$('.img-11').addClass('unavailable');
				$('.img-11').css('cursor','default');
				$(".img-11").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshows->descripcion=='Defensa Central Izquierdo')
	@if ($fieldshows->registrados < $fieldshows->total)
				<script type="text/javascript">
				$('.img-3').removeClass('selectplayer');
				$('.img-3').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-3').removeClass('available');
				$('.img-3').addClass('unavailable');
				$('.img-3').css('cursor','default');
				$(".img-3").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshows->descripcion=='Defensa Central Derecho')
	@if ($fieldshows->registrados < $fieldshows->total)
				<script type="text/javascript">
				$('.img-2').removeClass('selectplayer');
				$('.img-2').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-2').removeClass('available');
				$('.img-2').addClass('unavailable');
				$('.img-2').css('cursor','default');
				$(".img-2").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshows->descripcion=='Lateral Volante Derecho')
	@if ($fieldshows->registrados < $fieldshows->total)
				<script type="text/javascript">
				$('.img-10').removeClass('selectplayer');
				$('.img-10').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-10').removeClass('available');
				$('.img-10').addClass('unavailable');
				$('.img-10').css('cursor','default');
				$(".img-10").attr('data-content', 'Agotado');
		</script>
	@endif
@elseif($fieldshows->descripcion=='Medio Ofensivo Izquierdo')
	@if ($fieldshows->registrados < $fieldshows->total)
				<script type="text/javascript">
				$('.img-4').removeClass('selectplayer');
				$('.img-4').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-4').removeClass('available');
				$('.img-4').addClass('unavailable');
				$('.img-4').css('cursor','default');
				$(".img-4").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshows->descripcion=='Medio de contención')
	@if ($fieldshows->registrados < $fieldshows->total)
				<script type="text/javascript">
				$('.img-9').removeClass('selectplayer');
				$('.img-9').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-9').removeClass('available');
				$('.img-9').addClass('unavailable');
				$('.img-9').css('cursor','default');
				$(".img-9").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshows->descripcion=='Medio Ofensivo Derecho')
	@if ($fieldshows->registrados < $fieldshows->total)
				<script type="text/javascript">
				$('.img-8').removeClass('selectplayer');
				$('.img-8').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-8').removeClass('available');
				$('.img-8').addClass('unavailable');
				$('.img-8').css('cursor','default');
				$(".img-8").attr('data-content', 'Agotado');
		</script>
	@endif
@elseif($fieldshows->descripcion=='Delantero Extremo Izquierdo')
	@if ($fieldshows->registrados < $fieldshows->total)
				<script type="text/javascript">
				$('.img-5').removeClass('selectplayer');
				$('.img-5').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-5').removeClass('available');
				$('.img-5').addClass('unavailable');
				$('.img-5').css('cursor','default');
				$(".img-5").attr('data-content', 'Agotado');
		</script>
	@endif
@elseif($fieldshows->descripcion=='Delantero Extremo Derecho')
	@if ($fieldshows->registrados < $fieldshows->total)
				<script type="text/javascript">
				$('.img-7').removeClass('selectplayer');
				$('.img-7').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-7').removeClass('available');
				$('.img-7').addClass('unavailable');
				$('.img-7').css('cursor','default');
				$(".img-7").attr('data-content', 'Agotado');
		</script>
	@endif
@endif

@endforeach
<ul class="list-inline" style="margin-top: 30px;text-align: center;">
  <li><div style="background:#1bb590;height:10px;width:10px;border-radius:50%;display:inline-block;"></div>&nbsp;Disponible</li>
  <li><div style="background:#929494;height:10px;width:10px;border-radius:50%;display:inline-block;"></div>&nbsp;Agotado</li>
  <li><div style="background:#ce0b26;height:10px;width:10px;border-radius:50%;display:inline-block;"></div>&nbsp;Tu selección</li>
</ul>
<hr>
<div style="border-radius: 50%;cursor:pointer">
<img class="img-1 available" style="margin-left:35px;height: 52px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Portero" value="Portero" data-placement="right">
<br><i style="color:#000;font-size: 12px;margin-left: 54px;">P</i>
</div>
<div style="border-radius: 50%;cursor:pointer">
<img class="img-2 available" style="margin-left:35px;height:52px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Defensa Central Derecho" value="Defensa Central Derecho" data-placement="right">
<br><i style="color:#000;font-size: 12px;margin-left: 54px;">DCD</i>
</div>
<div style="border-radius: 50%;cursor:pointer">
<img class="img-3 available" style="margin-left:35px;height: 52px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Defensa Central Izquierdo" value="Defensa Central Izquierdo" data-placement="right">
<br><i style="color:#000;font-size: 12px;margin-left: 54px;">DCI</i>
</div>
<div style="border-radius: 50%;cursor:pointer">
<img class="img-4 available" style="margin-left:35px;height: 52px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Medio Ofensivo Izquierdo" value="Medio Ofensivo Izquierdo" data-placement="right">
<br><i style="color:#000;font-size: 12px;margin-left: 54px;">MOI</i>
</div>
<div style="border-radius: 50%;cursor:pointer">
<img class="img-5 available" style="margin-left:35px;height: 52px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Delantero Extremo Izquierdo" value="Delantero Extremo Izquierdo" data-placement="right">
<br><i style="color:#000;font-size: 12px;margin-left: 54px;">DEI</i>
</div>
<div style="border-radius: 50%;cursor:pointer">
<img class="img-6 available" style="margin-left:35px;height: 52px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Delantero Centro" value="Delantero Centro" data-placement="right">
<br><i style="color:#000;font-size: 12px;margin-left: 54px;">DC</i>
</div>
<div style="border-radius: 50%;cursor:pointer">
<img class="img-7 available" style="margin-left:35px;height: 52px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Delantero Extremo Derecho" value="Delantero Extremo Derecho" data-placement="right">
<br><i style="color:#000;font-size: 12px;margin-left: 54px;">DED</i>
</div>
<div style="border-radius: 50%;cursor:pointer">
<img class="img-8 available" style="margin-left:35px;height: 52px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Medio Ofensivo Derecho" value="Medio Ofensivo Derecho" data-placement="right">
<br><i style="color:#000;font-size: 12px;margin-left: 54px;">MOD</i>
</div>
<div style="border-radius: 50%;cursor:pointer">
<img class="img-9 available" style="margin-left:35px;height: 52px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Medio de Contención" value="Medio de Contención" data-placement="right">
<br><i style="color:#000;font-size: 12px;margin-left: 54px;">MC</i>
</div>
<div style="border-radius: 50%;cursor:pointer">
<img class="img-10 available" style="margin-left:35px;height: 52px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Lateral Volante Derecho" value="Lateral Volante Derecho" data-placement="right">
<br><i style="color:#000;font-size: 12px;margin-left: 54px;">LVD</i>
</div>
<div style="border-radius: 50%;cursor:pointer">
<img class="img-11 available" style="margin-left:35px;height: 52px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Lateral Volante Izquierdo" value="Lateral Volante Izquierdo" data-placement="right">
<br><i style="color:#000;font-size: 12px;margin-left: 54px;">LVI</i>
</div>
<hr>
<center>Selecciona tu perfil</center>
<div style="text-align: center">
<br>
<input name="profile_selectedd" type="radio" id="select-rightt" class="with-gap radio-col-red" value="Diestro" checked>
<label for="select-rightt">Diestro</label>
<input name="profile_selectedd" type="radio" id="select-leftt" class="with-gap radio-col-red" value="Zurdo" />
<label for="select-leftt">Zurdo</label>
</div>
@endif

@if (isset($result_players))
@foreach($result_players as $fieldshow)

@if ($fieldshow->descripcion=='Portero')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.img-1').removeClass('selectplayer');
				$('.img-1').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-1').removeClass('available');
				$('.img-1').addClass('unavailable');
				$('.img-1').css('cursor','default');
				$(".img-1").attr('data-content', 'Agotado');
				</script>
	@endif

@elseif ($fieldshow->descripcion=='Delantero centro')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.img-6').removeClass('selectplayer');
				$('.img-6').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-6').removeClass('available');
				$('.img-6').addClass('unavailable');
				$('.img-6').css('cursor','default');
				$(".img-6").attr('data-content', 'Agotado');
		</script>
	@endif
@elseif ($fieldshow->descripcion=='Lateral Volante Izquierdo')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.img-11').removeClass('selectplayer');
				$('.img-11').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-11').removeClass('available');
				$('.img-11').addClass('unavailable');
				$('.img-11').css('cursor','default');
				$(".img-11").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshow->descripcion=='Defensa Central Izquierdo')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.img-3').removeClass('selectplayer');
				$('.img-3').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-3').removeClass('available');
				$('.img-3').addClass('unavailable');
				$('.img-3').css('cursor','default');
				$(".img-3").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshow->descripcion=='Defensa Central Derecho')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.img-2').removeClass('selectplayer');
				$('.img-2').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-2').removeClass('available');
				$('.img-2').addClass('unavailable');
				$('.img-2').css('cursor','default');
				$(".img-2").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshow->descripcion=='Lateral Volante Derecho')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.img-10').removeClass('selectplayer');
				$('.img-10').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-10').removeClass('available');
				$('.img-10').addClass('unavailable');
				$('.img-10').css('cursor','default');
				$(".img-10").attr('data-content', 'Agotado');
		</script>
	@endif
@elseif($fieldshow->descripcion=='Medio Ofensivo Izquierdo')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.img-4').removeClass('selectplayer');
				$('.img-4').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-4').removeClass('available');
				$('.img-4').addClass('unavailable');
				$('.img-4').css('cursor','default');
				$(".img-4").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshow->descripcion=='Medio de contención')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.img-9').removeClass('selectplayer');
				$('.img-9').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-9').removeClass('available');
				$('.img-9').addClass('unavailable');
				$('.img-9').css('cursor','default');
				$(".img-9").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshow->descripcion=='Medio Ofensivo Derecho')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.img-8').removeClass('selectplayer');
				$('.img-8').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-8').removeClass('available');
				$('.img-8').addClass('unavailable');
				$('.img-8').css('cursor','default');
				$(".img-8").attr('data-content', 'Agotado');
		</script>
	@endif
@elseif($fieldshow->descripcion=='Delantero Extremo Izquierdo')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.img-5').removeClass('selectplayer');
				$('.img-5').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-5').removeClass('available');
				$('.img-5').addClass('unavailable');
				$('.img-5').css('cursor','default');
				$(".img-5").attr('data-content', 'Agotado');
		</script>
	@endif
@elseif($fieldshow->descripcion=='Delantero Extremo Derecho')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.img-7').removeClass('selectplayer');
				$('.img-7').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.img-7').removeClass('available');
				$('.img-7').addClass('unavailable');
				$('.img-7').css('cursor','default');
				$(".img-7").attr('data-content', 'Agotado');
		</script>
	@endif
@endif

@endforeach
@endif
<style type="text/css">

</style>



@if (isset($result_players))
<div id="display-field" style="display: block">
<ul class="list-unstyled list-option" style="margin-top: 16px;font-size: 13px;color: #6d6969;">
  <li><div style="background:#1bb590;height:10px;width:10px;border-radius:50%;display:inline-block;"></div>&nbsp;Disponible</li>
  <li><div style="background:#929494;height:10px;width:10px;border-radius:50%;display:inline-block;"></div>&nbsp;Agotado</li>
  <li><div style="background:#ce0b26;height:10px;width:10px;border-radius:50%;display:inline-block;"></div>&nbsp;Tu selección</li>
</ul>
<div style="margin-top:0px;position:relative;background: url('images/image.png');background-repeat: repeat;background-size: auto auto;height:318px;background-size: contain;background-repeat: no-repeat;width:530px;margin-left: 102px;">
<div style="position: absolute;border-radius: 50%;left:240px;top: 213px;cursor:pointer">
<div style="bottom: 311px;position: absolute;width: 168px;left: -50px;font-weight: bold;">POSICIÓN PREFERIDA</div>
<img class="img-1 available" style="height: 46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Portero" value="Portero" data-placement="top"><br><center><i style="color:#fff">P</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:321px;top:186px;cursor:pointer">
<img class="img-2 available" style="height:46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Defensa Central Derecho" value="Defensa Central Derecho" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">DCD</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:162px;top:186px;cursor:pointer">
<img class="img-3 available" style="height: 46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Defensa Central Izquierdo" value="Defensa Central Izquierdo" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">DCI</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left: 127px;top:89px;cursor:pointer">
<img class="img-4 available" style="height: 46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Medio Ofensivo Izquierdo" value="Medio Ofensivo Izquierdo" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">MOI</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:137px;top:28px;cursor:pointer">
<img class="img-5 available" style="height:46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Delantero Extremo Izquierdo" value="Delantero Extremo Izquierdo" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">DEI</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:242px;top:28px;cursor:pointer">
<img class="img-6 available" style="height:46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Delantero Centro" value="Delantero Centro" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">DC</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:346px;top:28px;cursor:pointer">
<img class="img-7 available" style="height:46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Delantero Extremo Derecho" value="Delantero Extremo Derecho" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">DED</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:360px;top:89px;cursor:pointer">
<img class="img-8 available" style="height: 46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Medio Ofensivo Derecho" value="Medio Ofensivo Derecho" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;;font-size: 12px;">MOD</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:242px;top:112px;cursor:pointer">
<img class="img-9 available" style="height: 46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Medio de Contención" value="Medio de Contención" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">MC</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:379px;top:164px;cursor:pointer">
<img class="img-10 available" style="height: 46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Lateral Volante Derecho" value="Lateral Volante Derecho" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">LVD</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:104px;top:163px;cursor:pointer">
<img class="img-11 available" style="height:46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Lateral Volante Izquierdo" value="Lateral Volante Izquierdo" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">LVI</i></center>
</div>
</div>
</div>
<div id="display-field2" style="display: none">


<div style="margin-top:90px;position:relative;background: url('images/image.png');background-repeat: repeat;background-size: auto auto;height:318px;background-size: contain;background-repeat: no-repeat;width:530px;margin-left: 102px;">
<div style="position: absolute;border-radius: 50%;left:240px;top: 213px;cursor:pointer">
<div style="bottom: 311px;position: absolute;width: 168px;left: -50px;font-weight: bold;">POSICIÓN ALTERNA</div>
<img class="images-01 available selectplayer" style="height: 46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Portero" value="Portero" data-placement="top"><br><center><i style="color:#fff">P</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:321px;top:186px;cursor:pointer">
<img class="images-02 available" style="height:46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Defensa Central Derecho" value="Defensa Central Derecho" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">DCD</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:162px;top:186px;cursor:pointer">
<img class="images-03 available" style="height: 46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Defensa Central Izquierdo" value="Defensa Central Izquierdo" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">DCI</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left: 127px;top:89px;cursor:pointer">
<img class="images-04 available" style="height: 46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Medio Ofensivo Izquierdo" value="Medio Ofensivo Izquierdo" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">MOI</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:137px;top:28px;cursor:pointer">
<img class="images-05 available" style="height:46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Delantero Extremo Izquierdo" value="Delantero Extremo Izquierdo" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">DEI</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:242px;top:28px;cursor:pointer">
<img class="images-06 available" style="height:46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Delantero Centro" value="Delantero Centro" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">DC</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:346px;top:28px;cursor:pointer">
<img class="images-07 available" style="height:46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Delantero Extremo Derecho" value="Delantero Extremo Derecho" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">DED</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:360px;top:89px;cursor:pointer">
<img class="images-08 available" style="height: 46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Medio Ofensivo Derecho" value="Medio Ofensivo Derecho" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;;font-size: 12px;">MOD</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:242px;top:112px;cursor:pointer">
<img class="images-09 available" style="height: 46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Medio de Contención" value="Medio de Contención" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">MC</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:379px;top:164px;cursor:pointer">
<img class="images-010 available" style="height: 46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Lateral Volante Derecho" value="Lateral Volante Derecho" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">LVD</i></center>
</div>
<div style="position: absolute;border-radius: 50%;left:104px;top:163px;cursor:pointer">
<img class="images-011 available" style="height:46px;border-radius: 50%;" src='images/posicion.svg' href="#" tabindex="0" data-toggle="popover" data-trigger="focus" title="" 
data-content="Lateral Volante Izquierdo" value="Lateral Volante Izquierdo" data-placement="top">
<br><center><i style="color:#fff;font-size: 12px;">LVI</i></center>
</div>
</div>
</div>
<div style="top: 400px;left: 353px;position: absolute;">
<input name="fields" type="radio" id="field1" class="with-gap radio-col-red" value="field1" checked>
<label for="field1"></label>
<input name="fields" type="radio" id="field2" class="with-gap radio-col-red" value="field2" />
<label for="field2"></label>	
</div>
<div style="position: absolute;top: 457px;left: 234px;">
Selecciona tu perfil:
<input name="profile_selected" type="radio" id="select-right" class="with-gap radio-col-red" value="Derecho" checked />
<label for="select-right"><img src="images/derecho-01.png" id="position-right"style="width: 29px;bottom: 21px;left: 35px;position: absolute;">Derecho</label>
<input name="profile_selected" type="radio" id="select-left" class="with-gap radio-col-red" value="Izquierdo" />
<label for="select-left"><img src="images/izquierdo.png" id="position-left" style="width: 29px;bottom: 21px;left: 35px;position: absolute;">Izquierdo</label>
</div>
@endif
<script type="text/javascript">
	var haveclass1 = $('.img-1').hasClass('unavailable');
	var haveclass2 = $('.img-2').hasClass('unavailable');
	var haveclass3 = $('.img-3').hasClass('unavailable');
	var haveclass4 = $('.img-4').hasClass('unavailable');
	var haveclass5 = $('.img-5').hasClass('unavailable');
	var haveclass6 = $('.img-6').hasClass('unavailable');
	var haveclass7 = $('.img-7').hasClass('unavailable');
	var haveclass8 = $('.img-8').hasClass('unavailable');
	var haveclass9 = $('.img-9').hasClass('unavailable');
	var haveclass10 = $('.img-10').hasClass('unavailable');
	var haveclass11 = $('.img-11').hasClass('unavailable');
	if (haveclass1)
	{
		
		if(haveclass2)
		{
			if(haveclass3)
				{
					if(haveclass4)
					{
						if(haveclass5)
							{
								if(haveclass6)
									{
										if(haveclass7)
											{
												if(haveclass8)
													{
														if(haveclass9)
															{
																if(haveclass10)
																	{
																		if(haveclass11)
																			{	
																				document.getElementById("danger-boton").style.display = "none";
																				 
																			
																			}
																			else
																			{
																				$('.img-11').addClass('selectplayer');
																			}			
																	}	
																	else
																	{
																		$('.img-10').addClass('selectplayer');
																	}						
															}
															else
															{
																$('.img-9').addClass('selectplayer');
															}
								
													}
													else
													{
														$('.img-8').addClass('selectplayer');
													}
								
											}
											else
											{
												$('.img-7').addClass('selectplayer');
											}
								
									}
									else
									{
										$('.img-6').addClass('selectplayer');
									}

							}
							else
							{
								$('.img-5').addClass('selectplayer');
							}
					}
					else
					{
						$('.img-4').addClass('selectplayer');
					}

				}
				else
				{
					$('.img-3').addClass('selectplayer');
				}

		}
		else
		{
			$('.img-2').addClass('selectplayer');
		}
	}
	else
	{
		$('.img-1').addClass('selectplayer');
	}

</script>
<script>
$('[data-toggle="popover"]').popover();
</script>
<script type="text/javascript">
$('.img-1').click(function(){

var ifexistsclass = $('.img-1').hasClass('unavailable');
if (ifexistsclass)
{

}
else
{
	$('.img-1').addClass('selectplayer');
var ifexists2 = $('.img-2').hasClass('selectplayer');
var ifexists3 = $('.img-3').hasClass('selectplayer');
var ifexists4 = $('.img-4').hasClass('selectplayer');
var ifexists5 = $('.img-5').hasClass('selectplayer');
var ifexists6 = $('.img-6').hasClass('selectplayer');
var ifexists7 = $('.img-7').hasClass('selectplayer');
var ifexists8 = $('.img-8').hasClass('selectplayer');
var ifexists9 = $('.img-9').hasClass('selectplayer');
var ifexists10 = $('.img-10').hasClass('selectplayer');
var ifexists11 = $('.img-11').hasClass('selectplayer');
if (ifexists2)
{
	$('.img-2').removeClass('selectplayer');
}
if (ifexists3)
{
	$('.img-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.img-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.img-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.img-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.img-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.img-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.img-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.img-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.img-11').removeClass('selectplayer');
}
}
});
</script>


<script type="text/javascript">
$('.img-2').click(function(){
var ifexistsclass = $('.img-2').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.img-2').addClass('selectplayer');
var ifexists1 = $('.img-1').hasClass('selectplayer');
var ifexists3 = $('.img-3').hasClass('selectplayer');
var ifexists4 = $('.img-4').hasClass('selectplayer');
var ifexists5 = $('.img-5').hasClass('selectplayer');
var ifexists6 = $('.img-6').hasClass('selectplayer');
var ifexists7 = $('.img-7').hasClass('selectplayer');
var ifexists8 = $('.img-8').hasClass('selectplayer');
var ifexists9 = $('.img-9').hasClass('selectplayer');
var ifexists10 = $('.img-10').hasClass('selectplayer');
var ifexists11 = $('.img-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.img-1').removeClass('selectplayer');
}
if (ifexists3)
{
	$('.img-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.img-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.img-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.img-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.img-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.img-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.img-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.img-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.img-11').removeClass('selectplayer');
}
}
});
</script>


<script type="text/javascript">
$('.img-3').click(function(){
var ifexistsclass = $('.img-3').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.img-3').addClass('selectplayer');
var ifexists1 = $('.img-1').hasClass('selectplayer');
var ifexists2 = $('.img-2').hasClass('selectplayer');
var ifexists4 = $('.img-4').hasClass('selectplayer');
var ifexists5 = $('.img-5').hasClass('selectplayer');
var ifexists6 = $('.img-6').hasClass('selectplayer');
var ifexists7 = $('.img-7').hasClass('selectplayer');
var ifexists8 = $('.img-8').hasClass('selectplayer');
var ifexists9 = $('.img-9').hasClass('selectplayer');
var ifexists10 = $('.img-10').hasClass('selectplayer');
var ifexists11 = $('.img-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.img-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.img-2').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.img-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.img-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.img-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.img-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.img-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.img-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.img-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.img-11').removeClass('selectplayer');
}
}
});
</script>

<script type="text/javascript">
$('.img-4').click(function(){
var ifexistsclass = $('.img-4').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.img-4').addClass('selectplayer');
var ifexists1 = $('.img-1').hasClass('selectplayer');
var ifexists2 = $('.img-2').hasClass('selectplayer');
var ifexists3 = $('.img-3').hasClass('selectplayer');
var ifexists5 = $('.img-5').hasClass('selectplayer');
var ifexists6 = $('.img-6').hasClass('selectplayer');
var ifexists7 = $('.img-7').hasClass('selectplayer');
var ifexists8 = $('.img-8').hasClass('selectplayer');
var ifexists9 = $('.img-9').hasClass('selectplayer');
var ifexists10 = $('.img-10').hasClass('selectplayer');
var ifexists11 = $('.img-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.img-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.img-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.img-3').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.img-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.img-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.img-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.img-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.img-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.img-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.img-11').removeClass('selectplayer');
}
}
});
</script>

<script type="text/javascript">
$('.img-5').click(function(){
var ifexistsclass = $('.img-5').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.img-5').addClass('selectplayer');
var ifexists1 = $('.img-1').hasClass('selectplayer');
var ifexists2 = $('.img-2').hasClass('selectplayer');
var ifexists3 = $('.img-3').hasClass('selectplayer');
var ifexists4 = $('.img-4').hasClass('selectplayer');
var ifexists6 = $('.img-6').hasClass('selectplayer');
var ifexists7 = $('.img-7').hasClass('selectplayer');
var ifexists8 = $('.img-8').hasClass('selectplayer');
var ifexists9 = $('.img-9').hasClass('selectplayer');
var ifexists10 = $('.img-10').hasClass('selectplayer');
var ifexists11 = $('.img-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.img-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.img-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.img-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.img-4').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.img-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.img-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.img-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.img-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.img-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.img-11').removeClass('selectplayer');
}
}
});
</script>

<script type="text/javascript">
$('.img-6').click(function(){
var ifexistsclass = $('.img-6').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
var ifexistsclass = $('.img-6').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.img-6').addClass('selectplayer');
var ifexists1 = $('.img-1').hasClass('selectplayer');
var ifexists2 = $('.img-2').hasClass('selectplayer');
var ifexists3 = $('.img-3').hasClass('selectplayer');
var ifexists4 = $('.img-4').hasClass('selectplayer');
var ifexists5 = $('.img-5').hasClass('selectplayer');
var ifexists7 = $('.img-7').hasClass('selectplayer');
var ifexists8 = $('.img-8').hasClass('selectplayer');
var ifexists9 = $('.img-9').hasClass('selectplayer');
var ifexists10 = $('.img-10').hasClass('selectplayer');
var ifexists11 = $('.img-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.img-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.img-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.img-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.img-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.img-5').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.img-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.img-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.img-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.img-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.img-11').removeClass('selectplayer');
}
}
}
});
</script>




<script type="text/javascript">
$('.img-7').click(function(){
var ifexistsclass = $('.img-7').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.img-7').addClass('selectplayer');
var ifexists1 = $('.img-1').hasClass('selectplayer');
var ifexists2 = $('.img-2').hasClass('selectplayer');
var ifexists3 = $('.img-3').hasClass('selectplayer');
var ifexists4 = $('.img-4').hasClass('selectplayer');
var ifexists5 = $('.img-5').hasClass('selectplayer');
var ifexists6 = $('.img-6').hasClass('selectplayer');
var ifexists8 = $('.img-8').hasClass('selectplayer');
var ifexists9 = $('.img-9').hasClass('selectplayer');
var ifexists10 = $('.img-10').hasClass('selectplayer');
var ifexists11 = $('.img-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.img-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.img-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.img-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.img-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.img-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.img-6').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.img-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.img-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.img-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.img-11').removeClass('selectplayer');
}
}
});
</script>




<script type="text/javascript">
$('.img-8').click(function(){
var ifexistsclass = $('.img-8').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.img-8').addClass('selectplayer');
var ifexists1 = $('.img-1').hasClass('selectplayer');
var ifexists2 = $('.img-2').hasClass('selectplayer');
var ifexists3 = $('.img-3').hasClass('selectplayer');
var ifexists4 = $('.img-4').hasClass('selectplayer');
var ifexists5 = $('.img-5').hasClass('selectplayer');
var ifexists6 = $('.img-6').hasClass('selectplayer');
var ifexists7 = $('.img-7').hasClass('selectplayer');
var ifexists9 = $('.img-9').hasClass('selectplayer');
var ifexists10 = $('.img-10').hasClass('selectplayer');
var ifexists11 = $('.img-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.img-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.img-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.img-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.img-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.img-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.img-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.img-7').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.img-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.img-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.img-11').removeClass('selectplayer');
}
}
});
</script>



<script type="text/javascript">
$('.img-9').click(function(){
var ifexistsclass = $('.img-9').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.img-9').addClass('selectplayer');
var ifexists1 = $('.img-1').hasClass('selectplayer');
var ifexists2 = $('.img-2').hasClass('selectplayer');
var ifexists3 = $('.img-3').hasClass('selectplayer');
var ifexists4 = $('.img-4').hasClass('selectplayer');
var ifexists5 = $('.img-5').hasClass('selectplayer');
var ifexists6 = $('.img-6').hasClass('selectplayer');
var ifexists7 = $('.img-7').hasClass('selectplayer');
var ifexists8 = $('.img-8').hasClass('selectplayer');
var ifexists10 = $('.img-10').hasClass('selectplayer');
var ifexists11 = $('.img-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.img-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.img-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.img-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.img-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.img-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.img-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.img-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.img-8').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.img-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.img-11').removeClass('selectplayer');
}
}
});
</script>



<script type="text/javascript">
$('.img-10').click(function(){
var ifexistsclass = $('.img-10').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.img-10').addClass('selectplayer');
var ifexists1 = $('.img-1').hasClass('selectplayer');
var ifexists2 = $('.img-2').hasClass('selectplayer');
var ifexists3 = $('.img-3').hasClass('selectplayer');
var ifexists4 = $('.img-4').hasClass('selectplayer');
var ifexists5 = $('.img-5').hasClass('selectplayer');
var ifexists6 = $('.img-6').hasClass('selectplayer');
var ifexists7 = $('.img-7').hasClass('selectplayer');
var ifexists8 = $('.img-8').hasClass('selectplayer');
var ifexists9 = $('.img-9').hasClass('selectplayer');
var ifexists11 = $('.img-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.img-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.img-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.img-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.img-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.img-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.img-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.img-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.img-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.img-9').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.img-11').removeClass('selectplayer');
}
}
});
</script>



<script type="text/javascript">
$('.img-11').click(function(){
var ifexistsclass = $('.img-11').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.img-11').addClass('selectplayer');
var ifexists1 = $('.img-1').hasClass('selectplayer');
var ifexists2 = $('.img-2').hasClass('selectplayer');
var ifexists3 = $('.img-3').hasClass('selectplayer');
var ifexists4 = $('.img-4').hasClass('selectplayer');
var ifexists5 = $('.img-5').hasClass('selectplayer');
var ifexists6 = $('.img-6').hasClass('selectplayer');
var ifexists7 = $('.img-7').hasClass('selectplayer');
var ifexists8 = $('.img-8').hasClass('selectplayer');
var ifexists9 = $('.img-9').hasClass('selectplayer');
var ifexists10 = $('.img-10').hasClass('selectplayer');
if (ifexists1)
{
	$('.img-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.img-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.img-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.img-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.img-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.img-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.img-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.img-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.img-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.img-10').removeClass('selectplayer');
}
}
});
</script>




<script type="text/javascript">
$('.images-01').click(function(){

var ifexistsclass = $('.images-01').hasClass('unavailable');
if (ifexistsclass)
{

}
else
{
	$('.images-01').addClass('selectplayer');
var ifexists2 = $('.images-02').hasClass('selectplayer');
var ifexists3 = $('.images-03').hasClass('selectplayer');
var ifexists4 = $('.images-04').hasClass('selectplayer');
var ifexists5 = $('.images-05').hasClass('selectplayer');
var ifexists6 = $('.images-06').hasClass('selectplayer');
var ifexists7 = $('.images-07').hasClass('selectplayer');
var ifexists8 = $('.images-08').hasClass('selectplayer');
var ifexists9 = $('.images-09').hasClass('selectplayer');
var ifexists10 = $('.images-010').hasClass('selectplayer');
var ifexists11 = $('.images-011').hasClass('selectplayer');
if (ifexists2)
{
	$('.images-02').removeClass('selectplayer');
}
if (ifexists3)
{
	$('.images-03').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-04').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-05').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-06').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-07').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.imagess-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-09').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-010').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-011').removeClass('selectplayer');
}
}
});
</script>


<script type="text/javascript">
$('.images-02').click(function(){
var ifexistsclass = $('.images-02').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-02').addClass('selectplayer');
var ifexists1 = $('.images-01').hasClass('selectplayer');
var ifexists3 = $('.images-03').hasClass('selectplayer');
var ifexists4 = $('.images-04').hasClass('selectplayer');
var ifexists5 = $('.images-05').hasClass('selectplayer');
var ifexists6 = $('.images-06').hasClass('selectplayer');
var ifexists7 = $('.images-07').hasClass('selectplayer');
var ifexists8 = $('.images-08').hasClass('selectplayer');
var ifexists9 = $('.images-09').hasClass('selectplayer');
var ifexists10 = $('.images-010').hasClass('selectplayer');
var ifexists11 = $('.images-011').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-01').removeClass('selectplayer');
}
if (ifexists3)
{
	$('.images-03').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-04').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-05').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-06').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-07').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-08').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-09').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-010').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-011').removeClass('selectplayer');
}
}
});
</script>


<script type="text/javascript">
$('.images-03').click(function(){
var ifexistsclass = $('.images-03').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-03').addClass('selectplayer');
var ifexists1 = $('.images-01').hasClass('selectplayer');
var ifexists2 = $('.images-02').hasClass('selectplayer');
var ifexists4 = $('.images-04').hasClass('selectplayer');
var ifexists5 = $('.images-05').hasClass('selectplayer');
var ifexists6 = $('.images-06').hasClass('selectplayer');
var ifexists7 = $('.images-07').hasClass('selectplayer');
var ifexists8 = $('.images-08').hasClass('selectplayer');
var ifexists9 = $('.images-09').hasClass('selectplayer');
var ifexists10 = $('.images-010').hasClass('selectplayer');
var ifexists11 = $('.images-011').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-01').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-02').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-04').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-05').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-06').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-07').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-08').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-09').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-010').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-011').removeClass('selectplayer');
}
}
});
</script>

<script type="text/javascript">
$('.images-04').click(function(){
var ifexistsclass = $('.images-04').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-04').addClass('selectplayer');
var ifexists1 = $('.images-01').hasClass('selectplayer');
var ifexists2 = $('.images-02').hasClass('selectplayer');
var ifexists3 = $('.images-03').hasClass('selectplayer');
var ifexists5 = $('.images-05').hasClass('selectplayer');
var ifexists6 = $('.images-06').hasClass('selectplayer');
var ifexists7 = $('.images-07').hasClass('selectplayer');
var ifexists8 = $('.images-08').hasClass('selectplayer');
var ifexists9 = $('.images-09').hasClass('selectplayer');
var ifexists10 = $('.images-010').hasClass('selectplayer');
var ifexists11 = $('.images-011').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-01').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-02').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-03').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-05').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-06').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-07').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-08').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-09').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-010').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-011').removeClass('selectplayer');
}
}
});
</script>

<script type="text/javascript">
$('.images-05').click(function(){
var ifexistsclass = $('.images-05').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-05').addClass('selectplayer');
var ifexists1 = $('.images-01').hasClass('selectplayer');
var ifexists2 = $('.images-02').hasClass('selectplayer');
var ifexists3 = $('.images-03').hasClass('selectplayer');
var ifexists4 = $('.images-04').hasClass('selectplayer');
var ifexists6 = $('.images-06').hasClass('selectplayer');
var ifexists7 = $('.images-07').hasClass('selectplayer');
var ifexists8 = $('.images-08').hasClass('selectplayer');
var ifexists9 = $('.images-09').hasClass('selectplayer');
var ifexists10 = $('.images-010').hasClass('selectplayer');
var ifexists11 = $('.images-011').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-01').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-02').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-03').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-04').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-06').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-07').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-08').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-09').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-010').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-011').removeClass('selectplayer');
}
}
});
</script>

<script type="text/javascript">
$('.images-06').click(function(){
var ifexistsclass = $('.images-06').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
var ifexistsclass = $('.images-06').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-06').addClass('selectplayer');
var ifexists1 = $('.images-01').hasClass('selectplayer');
var ifexists2 = $('.images-02').hasClass('selectplayer');
var ifexists3 = $('.images-03').hasClass('selectplayer');
var ifexists4 = $('.images-04').hasClass('selectplayer');
var ifexists5 = $('.images-05').hasClass('selectplayer');
var ifexists7 = $('.images-07').hasClass('selectplayer');
var ifexists8 = $('.images-08').hasClass('selectplayer');
var ifexists9 = $('.images-09').hasClass('selectplayer');
var ifexists10 = $('.images-010').hasClass('selectplayer');
var ifexists11 = $('.images-011').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-01').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-02').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-03').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-04').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-05').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-07').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-08').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-09').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-010').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-011').removeClass('selectplayer');
}
}
}
});
</script>




<script type="text/javascript">
$('.images-07').click(function(){
var ifexistsclass = $('.images-07').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-07').addClass('selectplayer');
var ifexists1 = $('.images-01').hasClass('selectplayer');
var ifexists2 = $('.images-02').hasClass('selectplayer');
var ifexists3 = $('.images-03').hasClass('selectplayer');
var ifexists4 = $('.images-04').hasClass('selectplayer');
var ifexists5 = $('.images-05').hasClass('selectplayer');
var ifexists6 = $('.images-06').hasClass('selectplayer');
var ifexists8 = $('.images-08').hasClass('selectplayer');
var ifexists9 = $('.images-09').hasClass('selectplayer');
var ifexists10 = $('.images-010').hasClass('selectplayer');
var ifexists11 = $('.images-011').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-01').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-02').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-03').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-04').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-05').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-06').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-08').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-09').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-010').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-011').removeClass('selectplayer');
}
}
});
</script>




<script type="text/javascript">
$('.images-08').click(function(){
var ifexistsclass = $('.images-08').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-08').addClass('selectplayer');
var ifexists1 = $('.images-01').hasClass('selectplayer');
var ifexists2 = $('.images-02').hasClass('selectplayer');
var ifexists3 = $('.images-03').hasClass('selectplayer');
var ifexists4 = $('.images-04').hasClass('selectplayer');
var ifexists5 = $('.images-05').hasClass('selectplayer');
var ifexists6 = $('.images-06').hasClass('selectplayer');
var ifexists7 = $('.images-07').hasClass('selectplayer');
var ifexists9 = $('.images-09').hasClass('selectplayer');
var ifexists10 = $('.images-010').hasClass('selectplayer');
var ifexists11 = $('.images-011').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-01').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-02').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-03').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-04').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-05').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-06').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-07').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-09').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-010').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-011').removeClass('selectplayer');
}
}
});
</script>



<script type="text/javascript">
$('.images-09').click(function(){
var ifexistsclass = $('.images-09').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-09').addClass('selectplayer');
var ifexists1 = $('.images-01').hasClass('selectplayer');
var ifexists2 = $('.images-02').hasClass('selectplayer');
var ifexists3 = $('.images-03').hasClass('selectplayer');
var ifexists4 = $('.images-04').hasClass('selectplayer');
var ifexists5 = $('.images-05').hasClass('selectplayer');
var ifexists6 = $('.images-06').hasClass('selectplayer');
var ifexists7 = $('.images-07').hasClass('selectplayer');
var ifexists8 = $('.images-08').hasClass('selectplayer');
var ifexists10 = $('.images-010').hasClass('selectplayer');
var ifexists11 = $('.images-011').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-01').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-02').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-03').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-04').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-05').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-06').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-07').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-08').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-010').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-011').removeClass('selectplayer');
}
}
});
</script>



<script type="text/javascript">
$('.images-010').click(function(){
var ifexistsclass = $('.images-010').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-010').addClass('selectplayer');
var ifexists1 = $('.images-01').hasClass('selectplayer');
var ifexists2 = $('.images-02').hasClass('selectplayer');
var ifexists3 = $('.images-03').hasClass('selectplayer');
var ifexists4 = $('.images-04').hasClass('selectplayer');
var ifexists5 = $('.images-05').hasClass('selectplayer');
var ifexists6 = $('.images-06').hasClass('selectplayer');
var ifexists7 = $('.images-07').hasClass('selectplayer');
var ifexists8 = $('.images-08').hasClass('selectplayer');
var ifexists9 = $('.images-09').hasClass('selectplayer');
var ifexists11 = $('.images-011').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-01').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-02').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-03').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-04').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-05').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-06').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-07').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-08').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-09').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-011').removeClass('selectplayer');
}
}
});
</script>



<script type="text/javascript">
$('.images-011').click(function(){
var ifexistsclass = $('.images-011').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-011').addClass('selectplayer');
var ifexists1 = $('.images-01').hasClass('selectplayer');
var ifexists2 = $('.images-02').hasClass('selectplayer');
var ifexists3 = $('.images-03').hasClass('selectplayer');
var ifexists4 = $('.images-04').hasClass('selectplayer');
var ifexists5 = $('.images-05').hasClass('selectplayer');
var ifexists6 = $('.images-06').hasClass('selectplayer');
var ifexists7 = $('.images-07').hasClass('selectplayer');
var ifexists8 = $('.images-08').hasClass('selectplayer');
var ifexists9 = $('.images-09').hasClass('selectplayer');
var ifexists10 = $('.images-010').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-01').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-02').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-03').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-04').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-05').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-06').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-07').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-08').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-09').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-010').removeClass('selectplayer');
}
}
});
</script>













<script type="text/javascript">
	    $('.next-step2').click(function() {
	                 document.getElementById("content-body").style.display = "none";

                 document.getElementById("content-body").setAttribute('style', 'display:none !important');
if ( document.getElementById("step-first"))
{
                    document.getElementById("step-first").style.display = "none";
}
if ( document.getElementById("step-second"))
{
 document.getElementById("step-second").style.display = "block"; 
}  
 
 if (  document.getElementById("image"))
{
 document.getElementById("image").style.display = "block"; 
}  
 if (document.getElementById("form1"))
 {
 	document.getElementById("form1").style.display = "block";
 }

 if (document.getElementById("step-fourth"))
 {
 	document.getElementById("step-fourth").style.display = "none";
    document.getElementById("step-fourth").setAttribute('style', 'display:none !important'); 
 }
     
if (document.getElementById("calendar"))
 {
     document.getElementById("calendar").style.display = "none";
 }              

 if (document.getElementById("body-field"))
 {
        document.getElementById("body-field").style.display = "none"; 
        document.getElementById("body-field").setAttribute('style', 'display:none !important'); 
 }              
                   
  if (document.getElementById("botton-hidden"))
 {
                    document.getElementById("botton-hidden").removeAttribute('style', 'display:none !important'); 
                    document.getElementById("botton-hidden").style.display = "none";
 
 }                  
 



                
    });
</script>
<script type="text/javascript">
	$("input[name=profile_selected]").change(function(){
		var value = $(this).val();
		if (value=="Izquierdo")
		{
			var image = document.getElementById('position-left');
			image.src = "images/derecho-02.png";
			var image = document.getElementById('position-right');
			image.src = "images/derecho.png";
		}
				if (value=="Derecho")
		{
			var image = document.getElementById('position-left');
			image.src = "images/izquierdo.png";
			var image = document.getElementById('position-right');
			image.src = "images/derecho-01.png";
		}
	});
</script>
<script type="text/javascript">
	$("input[name=fields]").change(function(){
		var values = $(this).val();
		
		if (values=="field1")
		{
			document.getElementById("display-field").style.display="block";
			document.getElementById("display-field2").style.display="none";
			document.getElementById("danger-boton").style.marginTop = "425px";
		}
		if (values=="field2")
		{
			document.getElementById("display-field2").style.display="block";
			document.getElementById("display-field").style.display="none";
			document.getElementById("danger-boton").style.marginTop = "350px";
		}
	});
</script>
