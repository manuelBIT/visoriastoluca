@extends('User.Layout.layout')
@section('contenido')
<div class="col-md-12" id="register-success" style="display: none">
  <div class="col-md-12" id="content-register" style="text-align: center;margin-top: 117px;">
  </div>
</div>
<div class="col-md-6 visible-lg visible-md " id="image">
</div>
<div class="col-md-6" id="form1">

  <div class="col-md-offset-1 col-md-10">
    <div class="col-md-12" style="height: 20px"></div>
    <form id="form-register">
      {{ csrf_field() }}
      <div id="step-first" style="display:block">
        <h1 style="text-align: center">¡Regístrate!</h1>
        <div class="col-md-6">
          <div class="form-group">
            <label>Estado de nacimiento</label>
              <select name="city" id="city" class="form-control">
                <option value="">Selecciona una opción</option>
                  @foreach ($states as $state)
                <option value="{{ $state->id }}">{{ $state->name }}</option>
                  @endforeach
              </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Municipio de nacimiento</label>
              <select name="towns" id="towns" class="form-control">
                <option value="">Selecciona una opción</option>
              </select>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>Nombre(s)</label>
            <input type="text" name="name" id="name" class="form-control" placeholder="">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
          <label>Apellido Paterno</label>
          <input type="text" name="apat_name" id="apat_name" class="form-control" placeholder="">
          </div>     
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Apellido Materno</label>
            <input type="text" name="amat_name" id="amat_name" class="form-control" placeholder="">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <div class="input-daterange travel-date-group bottommargin-sm">
                <div class="ottommargin-sm">
                  <label for="">Fecha de nacimiento</label>
                  <input type="text" id="birthday" name="birthday"  class="form-control tleft default" placeholder="MM-DD-YYYY">
                </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="">CURP <a id="search-curp" href="https://consultas.curp.gob.mx/CurpSP/" target="_blank">(Consulta tu curp)</a></label>
            <input type="text" id="curp_user" name="curp_user" value="" class="form-control" placeholder="">
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <input name="sex" type="radio" id="male" class="with-gap radio-col-red" value="H" checked />
            <label for="male">Masculino</label>
            <input name="sex" type="radio" id="female" class="with-gap radio-col-red" value="M" />
            <label for="female">Femenino</label>
          </div>
        </div>
        <div class="col-md-12">
          {!! Recaptcha::render() !!}
          <center><span id="alert-error" style="color:red"></span></center><br>
        </div>
        <div class="col-md-12">
          <div class="form-group" style="text-align: center">
            <button type="button" class="btn btn-form  next-step4">Elegir mi posición&nbsp;<i class="fa fa-chevron-right"></i></button>
          </div>
        </div>
      </div>   

      <div id="step-second" style="display: none">
        <h1 style="text-align: center;font-size: 25px">¡Muy bien! Casi terminas, solo necesitamos saber un poco más de ti</h1>
        <div class="col-md-6">
          <div class="form-group">
            <label>Nombre del padre o tutor</label>
            <input type="text" name="tutor" id="tutor" class="form-control" placeholder="">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Lugar de nacimiento</label>
            <input type="text" name="birthplace" id="birthplace" class="form-control" placeholder="">
          </div>
        </div>
        <div class="col-md-12">
          <div class="col-md-6">
            <div class="form-group">
              <label for="weight">Peso</label>
                <div class="white-section">
                  <input class="range_02" / id="weight">
                </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="height">Estatura</label>
                <div class="white-section">
                  <input class="range_03" / id="height">
                </div>
            </div>
          </div>
        </div>
        <div class="form-group" style="text-align: center">
          <button type="button" class="btn btn-form next-step3">Siguiente&nbsp<i class="fa fa-chevron-right"></i></button>
        </div>
      </div> 

      <div id="step-three" style="display: none">
        <h1 style="text-align: center">¡Regístrate!</h1>
        <div class="form-group">
          <label>Calle</label>
          <input type="text" name="street" id="street" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Colonia</label>
          <input type="text" name="colony" id="colony" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Codigo postal</label>
          <input type="text" name="postal_code" id="postal_code" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Correo electronico</label>
          <input type="text" name="email" id="email" class="form-control" placeholder="example@example.com">
        </div>
        <div class="form-group">
          <label>Teléfono</label>
          <input type="text" name="telephone" id="telephone" class="form-control" placeholder="(xx)xxxxxxxx">
        </div>
        <div class="form-group" style="text-align: center">
          <div style="color: red" id="error-email"></div><br>
          <button type="button" class="btn btn-form next-stepfinish">Finalizar</button>
        </div>
      </div> 
    </form>
  </div>
</div>
<div class="col-md-12" id="botton-hidden" style="display: none;">
  <button type="button" id="show-menu" style="display:none;">
    <i class="mdi mdi-arrow-left" id="icon-menu"></i>
  </button>
</div>

<div class="col-md-12" id="content-body" style="display: none">
  <div class="col-md-12" id="content-body2" style="display: none">
    <div class="col-md-4" style="padding-left: 0px;padding-right: 0px;background: rgba(255,255,255,0.9);border-top-left-radius: 8px;border-bottom-left-radius: 8px;">
      <div class="col-md-12" id="title-1" style="border-top-left-radius: 7px;">
        Selecciona tu visoria
      </div>
       <div class="col-md-12 menu-content" id="calendar" style="display:block">
    </div>
    <div class="menu-content" id="calendar2" style="display:none">
    </div> 
  </div> 
  <div class="col-md-8" style="padding-left: 0px;padding-right: 0px;border-top-right-radius: 7px;">
          <div class="col-md-12" id="title-2" style="border-top-right-radius: 7px;">
        Elige 2 posiciones en las que te gustaria jugar
      </div>
    <div class="col-md-12" id="step-fourth">
       <div class=" field-updated" id="field-updated" style="margin-left: 0px;"> 
      <div style="margin-top: 58px;position:relative;background: url('images/image.png');background-repeat: repeat;background-size: auto auto;height: 200px;background-size: contain;background-repeat: no-repeat;height: 333px;width: 616px;">
      </div>
    </div>
    </div>
  </div> 

  </div>
</div>


@stop