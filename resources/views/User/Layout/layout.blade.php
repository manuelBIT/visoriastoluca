<!DOCTYPE html>
<html style="height: 100%">
<head>
<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="css/materialdesignicons.min.css" type="text/css" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="shortcut icon" href="{{{ asset('images/favicon-tolucafc.png') }}}">
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons/et/et-line.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" href="css/fonts.css" type="text/css" />
	<link href="css/gsdk-bootstrap-wizard.css" rel="stylesheet" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap-datepicker.css" type="text/css" />
	<link rel="stylesheet" href="css/daterangepicker.css" type="text/css" />
	<link rel="stylesheet" href="css/main.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="css/perfect-scrollbar.css">
	<link rel="stylesheet" href="css/field/field.css" type="text/css" />
 	<link rel="stylesheet" href="css/ion.rangeslider.css" type="text/css" />

<style type="text/css">
	body{
		height: 100% !important;
	}
	html{
		height: 100% !important;
	}
</style>
	<title>Registro</title>


</head>

<body class="stretched" style="height: 100%">


@yield('contenido')
	<script src="js/perfect-scrollbar.js"></script>
		<script>
const ps = new PerfectScrollbar('#calendar');
</script>
  	<script src='js/jquery.js'></script>
	<script type="text/javascript" src="js/field/field.js"></script>
	<script type="text/javascript" src="js/plugins.js"></script>
<script src="js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
	<script src="js/additional-methods.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="js/curp.js"></script>
	<script src="js/gsdk-bootstrap-wizard.js"></script>
	<script src="js/rangeslider.min.js"></script>
	<script type="text/javascript" src="js/functions.js"></script>
	<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="locales/bootstrap-datepicker.es.min.js"></script>		
<script type="text/javascript">
	$(".range_02").ionRangeSlider({
				min: 0,
				max: 150,
				from: 67,
				postfix: 'kg',
});
	$(".range_03").ionRangeSlider({
				
				min: 1.0,
				max: 2.0,
				postfix: 'mts',
				from: 1.7,
				step: 0.01
});
</script>
<script type="text/javascript">
if( $('#step-first').is(":visible") ){
       	document.getElementById("botton-hidden").style.display = "none";
        document.getElementById("botton-hidden").setAttribute('style', 'display:none !important');	
}
</script>
	<script type="text/javascript">
	
			$('.travel-date-group .default').datepicker({
			
				language: "es",
				startDate: "01-01-1995",
				endDate: "31-12-2010",
				 autoclose: true,
				 format: "dd-mm-yyyy",
			 	defaultViewDate: { year: 1995, month: 01, day: 01 }
			});
	</script>
<script type="text/javascript">
	$("#city").change(function(event){
		$("#towns").empty();
		document.getElementById('towns').disabled = true;
			$("#towns").append("<option value=''>selecciona una opción</option>");
		$.get("towns/"+event.target.value+"",function(response,state){
			document.getElementById('towns').disabled = false;
			for(i=0; i<response.length;i++){
				$("#towns").append("<option value='"+response[i].id+"'>"+response[i].name+"</option>");
			}
		});
	})
</script>
<script type="text/javascript">
	$("#show-field").click(function(event){

			document.getElementById("show-field").style.display = "none";
        	document.getElementById("show-field").setAttribute('style', 'display:none !important');
    			document.getElementById("show-field2").style.display = "block";
        	document.getElementById("show-field2").setAttribute('style', 'display:block !important');
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var visitation_select = $('input:radio[name=date-selected]:checked').val();
$('input:radio[name=date-select]').removeAttr("checked");
             $.ajax({
        data:{visitation_select:visitation_select},
        url:   'http://localhost/visoriastoluca/public/availableteamoption',
        type:  'post',
        beforeSend:function(){
            $("#calendar2").html("<center><i class='fa fa-circle-o-notch fa-spin' style='font-size:24px;margin-top:170px'></i></center>");
        },
        success: function(response){
        	document.getElementById("field-updated").innerHTML = "";
                $("#calendar2").html(response);
         			$(".field-updated").html('<div style="margin-top: 58px;position:relative;background: url('+'images/image.png'+');background-repeat: repeat;background-size: auto auto;height: 200px;background-size: contain;background-repeat: no-repeat;height: 333px;width: 616px;">');

                $("#title-content").html("Selecciona tu posición");
       			document.getElementById("show-menu").style.display = "block";
        		document.getElementById("show-menu").setAttribute('style', 'display:block !important');	
            },
            error:function (xhr, ajaxOptions, thrownError){
               console.log(xhr.responseText);
            }
            });
	});

</script>

<script type="text/javascript">
$(window).resize(function(){
   var alto=$(window).height();
   var ancho=$(window).width();
   if (ancho<767)
   {
   	   	  var a = $('.radioselect').hasClass('radio-one');
   	   	  if (a)
   	   	  {
   	   	  	$('.radioselect').removeClass('radio-one');
   	   	  	
   	   	  }

   }

   if (ancho>767)
   {  	
   	var birthday = $("#birthday").val();
   $('.radioselect').addClass('radio-one');
            	var visitation_selected = $('.radioselect:checked').val();
   $.ajaxSetup({
    headers: {  
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
                 $.ajax({
        data:{birthday:birthday},
        url:   'http://localhost/visoriastoluca/public/availablevisitation',
        type:  'post',
        beforeSend:function(){
            $("#calendar").html("espere...");
        },
        success: function(response){
                $("#calendar").html(response);
            },
            error:function (xhr, ajaxOptions, thrownError){
           
            }
            });
              $.ajax({
        data:{visitation_selected:visitation_selected},
        url:   'http://localhost/visoriastoluca/public/availableteam',
        type:  'post',
        beforeSend:function(){
                 
            $(".field-updated").html("<i class='fa fa-circle-o-notch fa-spin' style='font-size:24px;margin-top:256px;margin-left:264px'></i>");
        },
        success: function(response){
				document.getElementById("field-updated").innerHTML = "";
                $(".field-updated").html(response);
            },
            error:function (xhr, ajaxOptions, thrownError){
              console.log(xhr.responseText);
            }
            });
   	if ( document.getElementById("show-field")) {
      document.getElementById("show-field").style.display = "none";
}
   	if ( document.getElementById("show-field2")) {
      document.getElementById("show-field2").style.display = "none";
}
   	if ( document.getElementById("show-menu")) {
     document.getElementById("show-menu").style.display = "none";
   document.getElementById("show-menu").setAttribute('style', 'display:none !important');	
}
   					
        
var birthday = $("#birthday").val();
 /* $.ajaxSetup({
    headers: {  
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
		              $.ajax({
        data:{birthday:birthday},
        url:   '/availablevisitations',
        type:  'post',
        beforeSend:function(){
            $("#calendar2").html("espere...");
        },
        success: function(response){
                $("#calendar2").html(response);
            },
            error:function (xhr, ajaxOptions, thrownError){
               alert("anchaaao");
            }
            });*/
   }
})
</script>




<script type="text/javascript">
	$("#show-menu").click(function(event){

			document.getElementById("show-field").style.display = "block";
        	document.getElementById("show-field").setAttribute('style', 'display:block !important');
    			document.getElementById("show-field2").style.display = "none";
        	document.getElementById("show-field2").setAttribute('style', 'display:none !important');
			document.getElementById("show-menu").style.display = "none";
        	document.getElementById("show-menu").setAttribute('style', 'display:none !important');	
        	document.getElementById("show-field").removeAttribute('style', 'display:none !important'); 
var birthday = $("#birthday").val();

		              $.ajax({
        data:{birthday:birthday},
        url:   'http://localhost/visoriastoluca/public/availablevisitations',
        type:  'post',
        beforeSend:function(){
            $("#calendar2").html("espere...");
        },
        success: function(response){
                $("#calendar2").html(response);
            },
            error:function (xhr, ajaxOptions, thrownError){
               console.log(xhr.responseText);
        console.log(thrownError);
            }
            });
});
</script>
</body>
</html>