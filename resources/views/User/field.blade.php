@if (isset($result_players))
@foreach($result_players as $fieldshow)

@if ($fieldshow->descripcion=='Portero')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.images-1').removeClass('selectplayer');
				$('.images-1').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.images-1').removeClass('available');
				$('.images-1').addClass('unavailable');
				$('.images-1').css('cursor','default');
				$(".images-1").attr('data-content', 'Agotado');
				</script>
	@endif

@elseif ($fieldshow->descripcion=='Delantero centro')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.images-9').removeClass('selectplayer');
				$('.images-9').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.images-9').removeClass('available');
				$('.images-9').addClass('unavailable');
				$('.images-9').css('cursor','default');
				$(".images-9").attr('data-content', 'Agotado');
		</script>
	@endif
@elseif ($fieldshow->descripcion=='Lateral Volante Izquierdo')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.images-5').removeClass('selectplayer');
				$('.images-5').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.images-5').removeClass('available');
				$('.images-5').addClass('unavailable');
				$('.images-5').css('cursor','default');
				$(".images-5").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshow->descripcion=='Defensa Central Izquierdo')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.images-4').removeClass('selectplayer');
				$('.images-4').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.images-4').removeClass('available');
				$('.images-4').addClass('unavailable');
				$('.images-4').css('cursor','default');
				$(".images-4").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshow->descripcion=='Defensa Central Derecho')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.images-3').removeClass('selectplayer');
				$('.images-3').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.images-3').removeClass('available');
				$('.images-3').addClass('unavailable');
				$('.images-3').css('cursor','default');
				$(".images-3").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshow->descripcion=='Lateral Volante Derecho')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.images-2').removeClass('selectplayer');
				$('.images-2').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.images-2').removeClass('available');
				$('.images-2').addClass('unavailable');
				$('.images-2').css('cursor','default');
				$(".images-2").attr('data-content', 'Agotado');
		</script>
	@endif
@elseif($fieldshow->descripcion=='Medio Ofensivo Izquierdo')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.images-10').removeClass('selectplayer');
				$('.images-10').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.images-10').removeClass('available');
				$('.images-10').addClass('unavailable');
				$('.images-10').css('cursor','default');
				$(".images-10").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshow->descripcion=='Medio de contención')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.images-6').removeClass('selectplayer');
				$('.images-6').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.images-6').removeClass('available');
				$('.images-6').addClass('unavailable');
				$('.images-6').css('cursor','default');
				$(".images-6").attr('data-content', 'Agotado');
		</script>
	@endif

@elseif($fieldshow->descripcion=='Medio Ofensivo Derecho')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.images-8').removeClass('selectplayer');
				$('.images-8').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.images-8').removeClass('available');
				$('.images-8').addClass('unavailable');
				$('.images-8').css('cursor','default');
				$(".images-8").attr('data-content', 'Agotado');
		</script>
	@endif
@elseif($fieldshow->descripcion=='Delantero Extremo Izquierdo')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.images-11').removeClass('selectplayer');
				$('.images-11').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.images-11').removeClass('available');
				$('.images-11').addClass('unavailable');
				$('.images-11').css('cursor','default');
				$(".images-11").attr('data-content', 'Agotado');
		</script>
	@endif
@elseif($fieldshow->descripcion=='Delantero Extremo Derecho')
	@if ($fieldshow->registrados < $fieldshow->total)
				<script type="text/javascript">
				$('.images-7').removeClass('selectplayer');
				$('.images-7').addClass('available');
				</script>
			@else
				<script type="text/javascript">
				$('.images-7').removeClass('available');
				$('.images-7').addClass('unavailable');
				$('.images-7').css('cursor','default');
				$(".images-7").attr('data-content', 'Agotado');
		</script>
	@endif
@endif

@endforeach
@endif

<center><ul class="list-inline " style="margin-top: 30px;text-align: center;">
  <li><div style="background:#1bb590;height:10px;width:10px;border-radius:50%;display:inline-block;"></div>&nbsp;Disponible</li>
  <li><div style="background:#929494;height:10px;width:10px;border-radius:50%;display:inline-block;"></div>&nbsp;Agotado</li>
  <li><div style="background:#ce0b26;height:10px;width:10px;border-radius:50%;display:inline-block;"></div>&nbsp;Tu selección</li>
</ul>
</center>
<center><h4>Posicion Preferida</h4></center>
						<div id="related-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="20" data-nav="false"  data-items-xxs="2" data-items-xs="4" data-items-lg="5">

							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="images-1 available" src="images/posicion.svg" alt="Open Imagination">
										</a>

									</div>
									<div class="portfolio-desc">
										<h3>Portero</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="images-2 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Lateral Volante Derecho</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="images-3 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Defensa Central Derecho</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="images-4 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Defensa Central Izquierdo</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="images-5 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Lateral Volante Izquierdo</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="images-6 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Medio de Contención</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="images-7 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Delantero Extremo Derecho</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="images-8 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Medio Ofensivo Derecho</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="images-9 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Delantero Centro</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="images-10 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Medio Ofensivo Izquierdo</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="images-11 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Delantero Extremo Izquierdo</h3>
									</div>
								</div>
							</div>
						</div>



<center><h4>Posicion Alternativa</h4></center>
						<div id="related-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="20" data-nav="false"  data-items-xxs="2" data-items-xs="4" data-items-lg="5">

							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="imagess-1 available" src="images/posicion.svg" alt="Open Imagination">
										</a>

									</div>
									<div class="portfolio-desc">
										<h3>Portero</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="imagess-2 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Lateral Volante Derecho</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="imagess-3 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Defensa Central Derecho</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="imagess-4 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Defensa Central Izquierdo</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="imagess-5 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Lateral Volante Izquierdo</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="imagess-6 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Medio de Contención</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="imagess-7 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Delantero Extremo Derecho</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="imagess-8 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Medio Ofensivo Derecho</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="imagess-9 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Delantero Centro</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="imagess-10 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Medio Ofensivo Izquierdo</h3>
									</div>
								</div>
							</div>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="#">
											<img class="imagess-11 available" src="images/posicion.svg" alt="Open Imagination">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3>Delantero Extremo Izquierdo</h3>
									</div>
								</div>
							</div>
						</div>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/plugins.js"></script>
	<script type="text/javascript" src="js/functions.js"></script>
	<script type="text/javascript">
	var haveclass1 = $('.images-1').hasClass('unavailable');
	var haveclass2 = $('.images-2').hasClass('unavailable');
	var haveclass3 = $('.images-3').hasClass('unavailable');
	var haveclass4 = $('.images-4').hasClass('unavailable');
	var haveclass5 = $('.images-5').hasClass('unavailable');
	var haveclass6 = $('.images-6').hasClass('unavailable');
	var haveclass7 = $('.images-7').hasClass('unavailable');
	var haveclass8 = $('.images-8').hasClass('unavailable');
	var haveclass9 = $('.images-9').hasClass('unavailable');
	var haveclass10 = $('.images-10').hasClass('unavailable');
	var haveclass11 = $('.images-11').hasClass('unavailable');
	if (haveclass1)
	{
		
		if(haveclass2)
		{
			if(haveclass3)
				{
					if(haveclass4)
					{
						if(haveclass5)
							{
								if(haveclass6)
									{
										if(haveclass7)
											{
												if(haveclass8)
													{
														if(haveclass9)
															{
																if(haveclass10)
																	{
																		if(haveclass11)
																			{	
																				document.getElementById("danger-boton").style.display = "none";
																				document.getElementById("show-field").style.display = "none";
																				document.getElementById("show-field").setAttribute('style', 'display:none !important');  
																			
																			}
																			else
																			{
																				$('.images-11').addClass('selectplayer');
																			}			
																	}	
																	else
																	{
																		$('.images-10').addClass('selectplayer');
																	}						
															}
															else
															{
																$('.images-9').addClass('selectplayer');
															}
								
													}
													else
													{
														$('.images-8').addClass('selectplayer');
													}
								
											}
											else
											{
												$('.images-7').addClass('selectplayer');
											}
								
									}
									else
									{
										$('.images-6').addClass('selectplayer');
									}

							}
							else
							{
								$('.images-5').addClass('selectplayer');
							}
					}
					else
					{
						$('.images-4').addClass('selectplayer');
					}

				}
				else
				{
					$('.images-3').addClass('selectplayer');
				}

		}
		else
		{
			$('.images-2').addClass('selectplayer');
		}
	}
	else
	{
		$('.images-1').addClass('selectplayer');
	}

</script>

<script type="text/javascript">
$('.images-1').click(function(){

var ifexistsclass = $('.images-1').hasClass('unavailable');
if (ifexistsclass)
{

}
else
{
	$('.images-1').addClass('selectplayer');
var ifexists2 = $('.images-2').hasClass('selectplayer');
var ifexists3 = $('.images-3').hasClass('selectplayer');
var ifexists4 = $('.images-4').hasClass('selectplayer');
var ifexists5 = $('.images-5').hasClass('selectplayer');
var ifexists6 = $('.images-6').hasClass('selectplayer');
var ifexists7 = $('.images-7').hasClass('selectplayer');
var ifexists8 = $('.images-8').hasClass('selectplayer');
var ifexists9 = $('.images-9').hasClass('selectplayer');
var ifexists10 = $('.images-10').hasClass('selectplayer');
var ifexists11 = $('.images-11').hasClass('selectplayer');
if (ifexists2)
{
	$('.images-2').removeClass('selectplayer');
}
if (ifexists3)
{
	$('.images-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-11').removeClass('selectplayer');
}
}
});
</script>


<script type="text/javascript">
$('.images-2').click(function(){
var ifexistsclass = $('.images-2').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-2').addClass('selectplayer');
var ifexists1 = $('.images-1').hasClass('selectplayer');
var ifexists3 = $('.images-3').hasClass('selectplayer');
var ifexists4 = $('.images-4').hasClass('selectplayer');
var ifexists5 = $('.images-5').hasClass('selectplayer');
var ifexists6 = $('.images-6').hasClass('selectplayer');
var ifexists7 = $('.images-7').hasClass('selectplayer');
var ifexists8 = $('.images-8').hasClass('selectplayer');
var ifexists9 = $('.images-9').hasClass('selectplayer');
var ifexists10 = $('.images-10').hasClass('selectplayer');
var ifexists11 = $('.images-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-1').removeClass('selectplayer');
}
if (ifexists3)
{
	$('.images-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-11').removeClass('selectplayer');
}
}
});
</script>


<script type="text/javascript">
$('.images-3').click(function(){
var ifexistsclass = $('.images-3').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-3').addClass('selectplayer');
var ifexists1 = $('.images-1').hasClass('selectplayer');
var ifexists2 = $('.images-2').hasClass('selectplayer');
var ifexists4 = $('.images-4').hasClass('selectplayer');
var ifexists5 = $('.images-5').hasClass('selectplayer');
var ifexists6 = $('.images-6').hasClass('selectplayer');
var ifexists7 = $('.images-7').hasClass('selectplayer');
var ifexists8 = $('.images-8').hasClass('selectplayer');
var ifexists9 = $('.images-9').hasClass('selectplayer');
var ifexists10 = $('.images-10').hasClass('selectplayer');
var ifexists11 = $('.images-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-2').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-11').removeClass('selectplayer');
}
}
});
</script>

<script type="text/javascript">
$('.images-4').click(function(){
var ifexistsclass = $('.images-4').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-4').addClass('selectplayer');
var ifexists1 = $('.images-1').hasClass('selectplayer');
var ifexists2 = $('.images-2').hasClass('selectplayer');
var ifexists3 = $('.images-3').hasClass('selectplayer');
var ifexists5 = $('.images-5').hasClass('selectplayer');
var ifexists6 = $('.images-6').hasClass('selectplayer');
var ifexists7 = $('.images-7').hasClass('selectplayer');
var ifexists8 = $('.images-8').hasClass('selectplayer');
var ifexists9 = $('.images-9').hasClass('selectplayer');
var ifexists10 = $('.images-10').hasClass('selectplayer');
var ifexists11 = $('.images-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-3').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-11').removeClass('selectplayer');
}
}
});
</script>

<script type="text/javascript">
$('.images-5').click(function(){
var ifexistsclass = $('.images-5').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-5').addClass('selectplayer');
var ifexists1 = $('.images-1').hasClass('selectplayer');
var ifexists2 = $('.images-2').hasClass('selectplayer');
var ifexists3 = $('.images-3').hasClass('selectplayer');
var ifexists4 = $('.images-4').hasClass('selectplayer');
var ifexists6 = $('.images-6').hasClass('selectplayer');
var ifexists7 = $('.images-7').hasClass('selectplayer');
var ifexists8 = $('.images-8').hasClass('selectplayer');
var ifexists9 = $('.images-9').hasClass('selectplayer');
var ifexists10 = $('.images-10').hasClass('selectplayer');
var ifexists11 = $('.images-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-4').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-11').removeClass('selectplayer');
}
}
});
</script>

<script type="text/javascript">
$('.images-6').click(function(){
var ifexistsclass = $('.images-6').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
var ifexistsclass = $('.images-6').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-6').addClass('selectplayer');
var ifexists1 = $('.images-1').hasClass('selectplayer');
var ifexists2 = $('.images-2').hasClass('selectplayer');
var ifexists3 = $('.images-3').hasClass('selectplayer');
var ifexists4 = $('.images-4').hasClass('selectplayer');
var ifexists5 = $('.images-5').hasClass('selectplayer');
var ifexists7 = $('.images-7').hasClass('selectplayer');
var ifexists8 = $('.images-8').hasClass('selectplayer');
var ifexists9 = $('.images-9').hasClass('selectplayer');
var ifexists10 = $('.images-10').hasClass('selectplayer');
var ifexists11 = $('.images-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-5').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-11').removeClass('selectplayer');
}
}
}
});
</script>




<script type="text/javascript">
$('.images-7').click(function(){
var ifexistsclass = $('.images-7').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-7').addClass('selectplayer');
var ifexists1 = $('.images-1').hasClass('selectplayer');
var ifexists2 = $('.images-2').hasClass('selectplayer');
var ifexists3 = $('.images-3').hasClass('selectplayer');
var ifexists4 = $('.images-4').hasClass('selectplayer');
var ifexists5 = $('.images-5').hasClass('selectplayer');
var ifexists6 = $('.images-6').hasClass('selectplayer');
var ifexists8 = $('.images-8').hasClass('selectplayer');
var ifexists9 = $('.images-9').hasClass('selectplayer');
var ifexists10 = $('.images-10').hasClass('selectplayer');
var ifexists11 = $('.images-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-6').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-11').removeClass('selectplayer');
}
}
});
</script>




<script type="text/javascript">
$('.images-8').click(function(){
var ifexistsclass = $('.images-8').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-8').addClass('selectplayer');
var ifexists1 = $('.images-1').hasClass('selectplayer');
var ifexists2 = $('.images-2').hasClass('selectplayer');
var ifexists3 = $('.images-3').hasClass('selectplayer');
var ifexists4 = $('.images-4').hasClass('selectplayer');
var ifexists5 = $('.images-5').hasClass('selectplayer');
var ifexists6 = $('.images-6').hasClass('selectplayer');
var ifexists7 = $('.images-7').hasClass('selectplayer');
var ifexists9 = $('.images-9').hasClass('selectplayer');
var ifexists10 = $('.images-10').hasClass('selectplayer');
var ifexists11 = $('.images-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-7').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-11').removeClass('selectplayer');
}
}
});
</script>



<script type="text/javascript">
$('.images-9').click(function(){
var ifexistsclass = $('.images-9').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-9').addClass('selectplayer');
var ifexists1 = $('.images-1').hasClass('selectplayer');
var ifexists2 = $('.images-2').hasClass('selectplayer');
var ifexists3 = $('.images-3').hasClass('selectplayer');
var ifexists4 = $('.images-4').hasClass('selectplayer');
var ifexists5 = $('.images-5').hasClass('selectplayer');
var ifexists6 = $('.images-6').hasClass('selectplayer');
var ifexists7 = $('.images-7').hasClass('selectplayer');
var ifexists8 = $('.images-8').hasClass('selectplayer');
var ifexists10 = $('.images-10').hasClass('selectplayer');
var ifexists11 = $('.images-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-8').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-11').removeClass('selectplayer');
}
}
});
</script>



<script type="text/javascript">
$('.images-10').click(function(){
var ifexistsclass = $('.images-10').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-10').addClass('selectplayer');
var ifexists1 = $('.images-1').hasClass('selectplayer');
var ifexists2 = $('.images-2').hasClass('selectplayer');
var ifexists3 = $('.images-3').hasClass('selectplayer');
var ifexists4 = $('.images-4').hasClass('selectplayer');
var ifexists5 = $('.images-5').hasClass('selectplayer');
var ifexists6 = $('.images-6').hasClass('selectplayer');
var ifexists7 = $('.images-7').hasClass('selectplayer');
var ifexists8 = $('.images-8').hasClass('selectplayer');
var ifexists9 = $('.images-9').hasClass('selectplayer');
var ifexists11 = $('.images-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-9').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.images-11').removeClass('selectplayer');
}
}
});
</script>



<script type="text/javascript">
$('.images-11').click(function(){
var ifexistsclass = $('.images-11').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.images-11').addClass('selectplayer');
var ifexists1 = $('.images-1').hasClass('selectplayer');
var ifexists2 = $('.images-2').hasClass('selectplayer');
var ifexists3 = $('.images-3').hasClass('selectplayer');
var ifexists4 = $('.images-4').hasClass('selectplayer');
var ifexists5 = $('.images-5').hasClass('selectplayer');
var ifexists6 = $('.images-6').hasClass('selectplayer');
var ifexists7 = $('.images-7').hasClass('selectplayer');
var ifexists8 = $('.images-8').hasClass('selectplayer');
var ifexists9 = $('.images-9').hasClass('selectplayer');
var ifexists10 = $('.images-10').hasClass('selectplayer');
if (ifexists1)
{
	$('.images-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.images-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.images-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.images-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.images-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.images-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.images-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.images-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.images-10').removeClass('selectplayer');
}
}
});
</script>












<script type="text/javascript">
$('.imagess-1').click(function(){

var ifexistsclass = $('.imagess-1').hasClass('unavailable');
if (ifexistsclass)
{

}
else
{
	$('.imagess-1').addClass('selectplayer');
var ifexists2 = $('.imagess-2').hasClass('selectplayer');
var ifexists3 = $('.imagess-3').hasClass('selectplayer');
var ifexists4 = $('.imagess-4').hasClass('selectplayer');
var ifexists5 = $('.imagess-5').hasClass('selectplayer');
var ifexists6 = $('.imagess-6').hasClass('selectplayer');
var ifexists7 = $('.imagess-7').hasClass('selectplayer');
var ifexists8 = $('.imagess-8').hasClass('selectplayer');
var ifexists9 = $('.imagess-9').hasClass('selectplayer');
var ifexists10 = $('.imagess-10').hasClass('selectplayer');
var ifexists11 = $('.imagess-11').hasClass('selectplayer');
if (ifexists2)
{
	$('.imagess-2').removeClass('selectplayer');
}
if (ifexists3)
{
	$('.imagess-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.imagess-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.imagess-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.imagess-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.imagess-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.imagess-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.imagess-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.imagess-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.imagess-11').removeClass('selectplayer');
}
}
});
</script>


<script type="text/javascript">
$('.imagess-2').click(function(){
var ifexistsclass = $('.imagess-2').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.imagess-2').addClass('selectplayer');
var ifexists1 = $('.imagess-1').hasClass('selectplayer');
var ifexists3 = $('.imagess-3').hasClass('selectplayer');
var ifexists4 = $('.imagess-4').hasClass('selectplayer');
var ifexists5 = $('.imagess-5').hasClass('selectplayer');
var ifexists6 = $('.imagess-6').hasClass('selectplayer');
var ifexists7 = $('.imagess-7').hasClass('selectplayer');
var ifexists8 = $('.imagess-8').hasClass('selectplayer');
var ifexists9 = $('.imagess-9').hasClass('selectplayer');
var ifexists10 = $('.imagess-10').hasClass('selectplayer');
var ifexists11 = $('.imagess-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.imagess-1').removeClass('selectplayer');
}
if (ifexists3)
{
	$('.imagess-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.imagess-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.imagess-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.imagess-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.imagess-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.imagess-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.imagess-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.imagess-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.imagess-11').removeClass('selectplayer');
}
}
});
</script>


<script type="text/javascript">
$('.imagess-3').click(function(){
var ifexistsclass = $('.imagess-3').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.imagess-3').addClass('selectplayer');
var ifexists1 = $('.imagess-1').hasClass('selectplayer');
var ifexists2 = $('.imagess-2').hasClass('selectplayer');
var ifexists4 = $('.imagess-4').hasClass('selectplayer');
var ifexists5 = $('.imagess-5').hasClass('selectplayer');
var ifexists6 = $('.imagess-6').hasClass('selectplayer');
var ifexists7 = $('.imagess-7').hasClass('selectplayer');
var ifexists8 = $('.imagess-8').hasClass('selectplayer');
var ifexists9 = $('.imagess-9').hasClass('selectplayer');
var ifexists10 = $('.imagess-10').hasClass('selectplayer');
var ifexists11 = $('.imagess-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.imagess-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.imagess-2').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.imagess-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.imagess-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.imagess-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.imagess-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.imagess-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.imagess-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.imagess-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.imagess-11').removeClass('selectplayer');
}
}
});
</script>

<script type="text/javascript">
$('.imagess-4').click(function(){
var ifexistsclass = $('.imagess-4').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.imagess-4').addClass('selectplayer');
var ifexists1 = $('.imagess-1').hasClass('selectplayer');
var ifexists2 = $('.imagess-2').hasClass('selectplayer');
var ifexists3 = $('.imagess-3').hasClass('selectplayer');
var ifexists5 = $('.imagess-5').hasClass('selectplayer');
var ifexists6 = $('.imagess-6').hasClass('selectplayer');
var ifexists7 = $('.imagess-7').hasClass('selectplayer');
var ifexists8 = $('.imagess-8').hasClass('selectplayer');
var ifexists9 = $('.imagess-9').hasClass('selectplayer');
var ifexists10 = $('.imagess-10').hasClass('selectplayer');
var ifexists11 = $('.imagess-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.imagess-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.imagess-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.imagess-3').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.imagess-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.imagess-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.imagess-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.imagess-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.imagess-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.imagess-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.imagess-11').removeClass('selectplayer');
}
}
});
</script>

<script type="text/javascript">
$('.imagess-5').click(function(){
var ifexistsclass = $('.imagess-5').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.imagess-5').addClass('selectplayer');
var ifexists1 = $('.imagess-1').hasClass('selectplayer');
var ifexists2 = $('.imagess-2').hasClass('selectplayer');
var ifexists3 = $('.imagess-3').hasClass('selectplayer');
var ifexists4 = $('.imagess-4').hasClass('selectplayer');
var ifexists6 = $('.imagess-6').hasClass('selectplayer');
var ifexists7 = $('.imagess-7').hasClass('selectplayer');
var ifexists8 = $('.imagess-8').hasClass('selectplayer');
var ifexists9 = $('.imagess-9').hasClass('selectplayer');
var ifexists10 = $('.imagess-10').hasClass('selectplayer');
var ifexists11 = $('.imagess-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.imagess-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.imagess-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.imagess-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.imagess-4').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.imagess-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.imagess-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.imagess-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.imagess-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.imagess-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.imagess-11').removeClass('selectplayer');
}
}
});
</script>

<script type="text/javascript">
$('.imagess-6').click(function(){
var ifexistsclass = $('.imagess-6').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
var ifexistsclass = $('.imagess-6').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.imagess-6').addClass('selectplayer');
var ifexists1 = $('.imagess-1').hasClass('selectplayer');
var ifexists2 = $('.imagess-2').hasClass('selectplayer');
var ifexists3 = $('.imagess-3').hasClass('selectplayer');
var ifexists4 = $('.imagess-4').hasClass('selectplayer');
var ifexists5 = $('.imagess-5').hasClass('selectplayer');
var ifexists7 = $('.imagess-7').hasClass('selectplayer');
var ifexists8 = $('.imagess-8').hasClass('selectplayer');
var ifexists9 = $('.imagess-9').hasClass('selectplayer');
var ifexists10 = $('.imagess-10').hasClass('selectplayer');
var ifexists11 = $('.imagess-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.imagess-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.imagess-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.imagess-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.imagess-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.imagess-5').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.imagess-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.imagess-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.imagess-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.imagess-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.imagess-11').removeClass('selectplayer');
}
}
}
});
</script>




<script type="text/javascript">
$('.imagess-7').click(function(){
var ifexistsclass = $('.imagess-7').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.imagess-7').addClass('selectplayer');
var ifexists1 = $('.imagess-1').hasClass('selectplayer');
var ifexists2 = $('.imagess-2').hasClass('selectplayer');
var ifexists3 = $('.imagess-3').hasClass('selectplayer');
var ifexists4 = $('.imagess-4').hasClass('selectplayer');
var ifexists5 = $('.imagess-5').hasClass('selectplayer');
var ifexists6 = $('.imagess-6').hasClass('selectplayer');
var ifexists8 = $('.imagess-8').hasClass('selectplayer');
var ifexists9 = $('.imagess-9').hasClass('selectplayer');
var ifexists10 = $('.imagess-10').hasClass('selectplayer');
var ifexists11 = $('.imagess-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.imagess-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.imagess-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.imagess-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.imagess-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.imagess-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.imagess-6').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.imagess-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.imagess-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.imagess-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.imagess-11').removeClass('selectplayer');
}
}
});
</script>




<script type="text/javascript">
$('.imagess-8').click(function(){
var ifexistsclass = $('.imagess-8').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.imagess-8').addClass('selectplayer');
var ifexists1 = $('.imagess-1').hasClass('selectplayer');
var ifexists2 = $('.imagess-2').hasClass('selectplayer');
var ifexists3 = $('.imagess-3').hasClass('selectplayer');
var ifexists4 = $('.imagess-4').hasClass('selectplayer');
var ifexists5 = $('.imagess-5').hasClass('selectplayer');
var ifexists6 = $('.imagess-6').hasClass('selectplayer');
var ifexists7 = $('.imagess-7').hasClass('selectplayer');
var ifexists9 = $('.imagess-9').hasClass('selectplayer');
var ifexists10 = $('.imagess-10').hasClass('selectplayer');
var ifexists11 = $('.imagess-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.imagess-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.imagess-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.imagess-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.imagess-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.imagess-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.imagess-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.imagess-7').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.imagess-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.imagess-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.imagess-11').removeClass('selectplayer');
}
}
});
</script>



<script type="text/javascript">
$('.imagess-9').click(function(){
var ifexistsclass = $('.imagess-9').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.imagess-9').addClass('selectplayer');
var ifexists1 = $('.imagess-1').hasClass('selectplayer');
var ifexists2 = $('.imagess-2').hasClass('selectplayer');
var ifexists3 = $('.imagess-3').hasClass('selectplayer');
var ifexists4 = $('.imagess-4').hasClass('selectplayer');
var ifexists5 = $('.imagess-5').hasClass('selectplayer');
var ifexists6 = $('.imagess-6').hasClass('selectplayer');
var ifexists7 = $('.imagess-7').hasClass('selectplayer');
var ifexists8 = $('.imagess-8').hasClass('selectplayer');
var ifexists10 = $('.imagess-10').hasClass('selectplayer');
var ifexists11 = $('.imagess-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.imagess-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.imagess-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.imagess-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.imagess-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.imagess-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.imagess-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.images-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.imagess-8').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.imagess-10').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.imagess-11').removeClass('selectplayer');
}
}
});
</script>



<script type="text/javascript">
$('.imagess-10').click(function(){
var ifexistsclass = $('.imagess-10').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.imagess-10').addClass('selectplayer');
var ifexists1 = $('.imagess-1').hasClass('selectplayer');
var ifexists2 = $('.imagess-2').hasClass('selectplayer');
var ifexists3 = $('.imagess-3').hasClass('selectplayer');
var ifexists4 = $('.imagess-4').hasClass('selectplayer');
var ifexists5 = $('.imagess-5').hasClass('selectplayer');
var ifexists6 = $('.imagess-6').hasClass('selectplayer');
var ifexists7 = $('.imagess-7').hasClass('selectplayer');
var ifexists8 = $('.imagess-8').hasClass('selectplayer');
var ifexists9 = $('.imagess-9').hasClass('selectplayer');
var ifexists11 = $('.imagess-11').hasClass('selectplayer');
if (ifexists1)
{
	$('.imagess-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.imagess-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.imagess-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.imagess-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.imagess-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.imagess-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.imagess-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.imagess-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.imagess-9').removeClass('selectplayer');
}
if (ifexists11)
{
	$('.imagess-11').removeClass('selectplayer');
}
}
});
</script>



<script type="text/javascript">
$('.imagess-11').click(function(){
var ifexistsclass = $('.imagess-11').hasClass('unavailable');
if (ifexistsclass)
{

}
else{
$('.imagess-11').addClass('selectplayer');
var ifexists1 = $('.imagess-1').hasClass('selectplayer');
var ifexists2 = $('.imagess-2').hasClass('selectplayer');
var ifexists3 = $('.imagess-3').hasClass('selectplayer');
var ifexists4 = $('.imagess-4').hasClass('selectplayer');
var ifexists5 = $('.imagess-5').hasClass('selectplayer');
var ifexists6 = $('.imagess-6').hasClass('selectplayer');
var ifexists7 = $('.imagess-7').hasClass('selectplayer');
var ifexists8 = $('.imagess-8').hasClass('selectplayer');
var ifexists9 = $('.imagess-9').hasClass('selectplayer');
var ifexists10 = $('.imagess-10').hasClass('selectplayer');
if (ifexists1)
{
	$('.imagess-1').removeClass('selectplayer');
}
if (ifexists2)
{
	$('.imagess-2').removeClass('selectplayer');
}

if (ifexists3)
{
	$('.imagess-3').removeClass('selectplayer');
}
if (ifexists4)
{
	$('.imagess-4').removeClass('selectplayer');
}
if (ifexists5)
{
	$('.imagess-5').removeClass('selectplayer');
}
if (ifexists6)
{
	$('.imagess-6').removeClass('selectplayer');
}
if (ifexists7)
{
	$('.imagess-7').removeClass('selectplayer');
}
if (ifexists8)
{
	$('.imagess-8').removeClass('selectplayer');
}
if (ifexists9)
{
	$('.imagess-9').removeClass('selectplayer');
}
if (ifexists10)
{
	$('.imagess-10').removeClass('selectplayer');
}
}
});
</script>