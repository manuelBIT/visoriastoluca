<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
    protected $table = "catmunicipio";
    protected $fillable = ['name','id_estado'];

    public function state(){
        return $this->hasOne('App\State');
    }
	public static function towns($id){

		return Town::where('id_estado','=',$id)
				->OrderBy('name')
				->get();
	}


}
