<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visoria extends Model
{
    protected $table = 'visorias';

    public function town(){
        return $this->hasOne('App\Town');
    }
}
