<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visoria;
use App\State;
use App\Town;

// class VisoriaController extends Controller
// {
//     public function index()
//     {
//         return view('Visoria.index');
//     }
// }

class VisoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Visorias = Visoria::latest()->paginate(5);
        return view('Visoria.index',compact('Visorias'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {	$states = State::all();
    	
    	return view('Visoria.create',compact('states'));
        // return view('Visoria.create')->with($name);
    }

    public function getTowns(Request $request,$id)
    {
        if ($request->ajax()){
            $towns = Town::towns($id);
            return response()->json($towns);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'Unidad' => 'required',
            'categoria' => 'required',
        ]);
        Visoria::create($request->all());
        return redirect()->route('Visorias.index')
                        ->with('success','Visoria created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Visoria = Visoria::find($id);
        return view('Visorias.show',compact('Visoria'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Visoria = Visoria::find($id);
        return view('Visorias.edit',compact('Visoria'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
        Visoria::find($id)->update($request->all());
        return redirect()->route('Visorias.index')
                        ->with('success','Visoria updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Visoria::find($id)->delete();
        return redirect()->route('Visorias.index')
                        ->with('success','Visoria deleted successfully');
    }
}