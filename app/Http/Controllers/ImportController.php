<?php

namespace App\Http\Controllers;
use App\Town;
use App\State;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class ImportController extends Controller
{
    public function import()
    {
    	Excel::load('CatMunicipios.csv',function($reader){
    		
    		$results = $reader->get();
    		Town::insert($results->toArray());


    	});
    	
    }
}
