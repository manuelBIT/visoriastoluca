<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterEmail;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    
    public function invoice(Request $request)
    {
    	$email = $request->input('email');
    	$name = $request->input('name');
    	$apat = $request->input('apat_name');
    	$amat = $request->input('amat_name');
      	$curp = $request->input('curp_user');    	
        Mail::to($email,$name)
        ->send(new RegisterEmail($name,$apat,$amat,$curp));
 		 return 1;
    }
    public function createdocument(Request $request)
    {

    	$email = $request->input('email');
		$name = $request->input('name');
		$apat = $request->input('apat_name');
		$amat = $request->input('amat_name');
		$birthday = $request->input('birthday');
		$position = $request->input('position');
		$weight = $request->input('weight');
		$tutor = $request->input('tutor');
		$telephone = $request->input('telephone');
		$birthplace = $request->input('birthplace');
		$height = $request->input('height');
		$street = $request->input('street');
		$colony = $request->input('colony');
		$citytext = $request->input('citytext');
		$postalcode  = $request->input('postalcode');
		$profile = $request->input('profile');
    	$view = view('pdf.invoice',compact('profile','postalcode','citytext','colony','street','height','email','apat','amat','name','birthday','position','weight','tutor','telephone','birthplace'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream();
    }
}
