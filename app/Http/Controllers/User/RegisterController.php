<?php

namespace App\Http\Controllers\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterEmail;
use Illuminate\Http\Request;
use App\User;
use App\Visoria;
use App\Http\Controllers\Controller;
Use Session;
use App\Town;
use App\State;
Use Redirect;
use DB;

class RegisterController extends Controller
{
    public function index()
    {
        $states = State::all();
    	return view('User.index',compact('states'));
    }

    public function findvisitation(Request $request)
    {
        $birthday = $request->input('birthday');
        $birthday = date("Y", strtotime($birthday)); 
        $find = Visoria::where('categoria','LIKE',"%$birthday%")->get();
        if(!$find->isEmpty())
        {
            return 0;
        }
        else
            {
                return 1;
            }
    }
    public function store(Request $request)
    {
        $email = $request->input('email');
        $name = $request->input('name');
        $apat = $request->input('apat_name');
        $amat = $request->input('amat_name');
        $curp = $request->input('curp_user');
        $user_find = User::where('email','=',$email)->get();
        if(!$user_find->isEmpty())
            {
                return 0;
            }
            else{
                    $user = new User;
                    $user->create([
                    'email' => $request->input('email'),
                    'id_estado'=>$request->input('city'),
                    'id_municipio'=>$request->input('town'),
                    'curp_user'=>$request->input('curp_user'),                    
                    'telephone' => $request->input('telephone'),
                    'street' => $request->input('street'),
                    'colony' => $request->input('colony'),
                    'postal_code' => $request->input('postal_code'),
                    'name' => $request->input('name'),
                    'apat_name' => $request->input('apat_name'),
                    'amat_name' => $request->input('amat_name'),
                    'tutor' => $request->input('tutor'),
                    'birthday' => $request->input('birthday'),
                    'birthplace' => $request->input('birthplace'),
                    'weight' => $request->input('weight'),
                    'height' => $request->input('height'),
                    'position' => $request->input('position'),
                    'profile' => $request->input('profile'),
                    ]);

        return 1;
                }

    }

    public function searchvisitation(Request $request)
    {
        $birthday = $request->input('birthday');
        $birthday = date("Y", strtotime($birthday)); 

    $date_find = Visoria::select(DB::raw("DISTINCT MONTH(fecha) as fecha"))
                    ->where('categoria','LIKE',"%$birthday%")
                    ->get();

$query = 
" (SELECT DISTINCT todo2.idmun,todo2.idest,todo2.municipio,todo2.estado,todo2.idv,todo2.unidad,todo2.fecha,todo2.categoria,todo2.hora".
" FROM".
" (".
" (SELECT todo.municipio,todo.registrados,todo.idest, todo.idmun,todo.total,todo.id AS idv,todo.estado,todo.unidad,".
" todo.fecha,todo.hora,todo.categoria". 
" FROM ( (SELECT DISTINCT visorias.id,catestado.id AS idest,catmunicipio.id". 
" AS idmun, Get_inscribed(visorias.id,cupos.descripcion)".
" AS registrados,cupos.total, catestado.name AS estado, catmunicipio.name".
" AS municipio,visorias.unidad, visorias.fecha,visorias.hora,visorias.categoria".
" FROM visorias INNER JOIN catestado ON visorias.id_estado = catestado.id".
" INNER JOIN catmunicipio ON visorias.id_municipio = catmunicipio.id INNER JOIN cupos ON visorias.id = cupos.id_visoria".
" WHERE visorias.categoria LIKE '%1995%'".
" ORDER BY fecha".
" ) AS todo".
" ) WHERE registrados < total".
" ORDER BY fecha".
" )) AS todo2".
" GROUP BY idmun".
" ORDER BY fecha)";


$visitation_find = DB::select(DB::raw($query));
        return view('User.visitationshow',compact('visitation_find','date_find'));
    }
    public function searchvisitationss(Request $request)
    {
        $birthday = $request->input('birthday');
        $birthday = date("Y", strtotime($birthday)); 

    $date_find = Visoria::select(DB::raw("DISTINCT MONTH(fecha) as fecha"))
                    ->where('categoria','LIKE',"%$birthday%")
                    ->get();
        $visitation_finds  = DB::table('visorias as v')
            ->Join('catestado as est','v.id_estado','=','est.id')
            ->Join('catmunicipio as mun','v.id_municipio','=','mun.id')
            ->Select('v.id','est.name as estado','mun.name as municipio','Unidad','fecha','hora','categoria')
            ->Where('v.categoria',"LIKE","%$birthday%")
            ->get();
                                    
        return view('User.visitationshow',compact('visitation_finds','date_find'));
    }
    public function visitationteam(Request $request)
    {
        $id = $request->input('visitation_selected');
        $resultfield = DB::table('detalle_visorias as detv')
            ->Join('visorias as v','detv.id_visoria','=','v.id')
            ->Join('users as u','detv.id_usuario','=','u.id')
            ->Select(DB::raw('COUNT(detv.descripcion) as ocupados'),'detv.id','detv.descripcion','detv.id_usuario','detv.id_visoria')
            ->Where('detv.id_visoria',$id)
            ->groupBy('detv.descripcion')
            ->get();

        $result_players = DB::table('visorias as v')
            ->join('cupos as c','v.id','=','c.id_visoria')
            ->select(DB::raw('Get_inscribed(v.id,c.descripcion) AS registrados'),'v.id','v.Unidad','v.fecha','v.hora','v.categoria','c.descripcion','c.total')
            ->where('v.id',$id)
            ->get();
        return view('User.fieldshow',compact('result_players'));
    }
    public function visitationteamview(Request $request)
    {
        $id = $request->input('visitation_selected');
        $resultfield = DB::table('detalle_visorias as detv')
            ->Join('visorias as v','detv.id_visoria','=','v.id')
            ->Join('users as u','detv.id_usuario','=','u.id')
            ->Select(DB::raw('COUNT(detv.descripcion) as ocupados'),'detv.id','detv.descripcion','detv.id_usuario','detv.id_visoria')
            ->Where('detv.id_visoria',$id)
            ->groupBy('detv.descripcion')
            ->get();

        $result_players = DB::table('visorias as v')
            ->join('cupos as c','v.id','=','c.id_visoria')
            ->select(DB::raw('Get_inscribed(v.id,c.descripcion) AS registrados'),'v.id','v.Unidad','v.fecha','v.hora','v.categoria','c.descripcion','c.total')
            ->where('v.id',$id)
            ->get();
        return view('User.field',compact('result_players'));
    }
    public function visitationteamoption(Request $request)
    {
         $id = $request->input('visitation_select');
        $result_playersoption = DB::table('visorias as v')
            ->join('cupos as c','v.id','=','c.id_visoria')
            ->select(DB::raw('Get_inscribed(v.id,c.descripcion) AS registrados'),'v.id','v.Unidad','v.fecha','v.hora','v.categoria','c.descripcion','c.total')
            ->where('v.id',$id)
            ->get();
        return view('User.fieldshow',compact('result_playersoption'));
    }
    public function getTowns(Request $request,$id)
    {
        if ($request->ajax()){
            $towns = Town::towns($id);
            return response()->json($towns);
        }
    }
    public function confirm(Request $request)
    {

        $email = $request->input('email');
        $name = $request->input('name');
        $apat = $request->input('apat_name');
        $amat = $request->input('amat_name');
        $birthday = $request->input('birthday');
        $position = $request->input('position');
        $weight = $request->input('weight');
        $tutor = $request->input('tutor');
        $telephone = $request->input('telephone');
        $birthplace = $request->input('birthplace');
        $height = $request->input('height');
        $street = $request->input('street');
        $colony = $request->input('colony');
        $citytext = $request->input('citytext');
        $postal_code = $request->input('postal_code');
        $profile = $request->input('profile');
        return view('pdf.template',compact('profile','postal_code','citytext','colony','street','height','email','name','apat','amat','birthday','position','weight','tutor','telephone','birthplace'));
    }
}
