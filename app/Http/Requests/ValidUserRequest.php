<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6'
            //
        ];
    }

    public function messages()
    {
        return [

            'name.required' => 'El nombre es obligatorio.',
            'email.required' => 'El correo es obligatorio.',
            'email.email' => 'El correo electronico no tiene una direccion valida.',
            'password.required' => 'La contraseña es obligatoria.',
            'password.min' => 'La contraseña debe tener como minimo 6 caracteres.',
            'email.unique' => 'El correo ya ha sido registrado.'
        ];
    }
}
