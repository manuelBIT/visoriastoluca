<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $apat;
    public $amat;
    public $curp;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$apat,$amat,$curp)
    {
        $this->name= $name;
        $this->apat= $apat;
        $this->amat= $amat;
        $this->curp = $curp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.RegisterEmail')
        ->from('josegdamians@gmail.com','Visorias Club Deportivo Toluca')
        ->subject('Registro Exitoso');
    }
}
