<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_municipio');   
            $table->integer('id_estado');    
            $table->string('Unidad');     
            $table->date('fecha');
            $table->time('hora');
            $table->string('categoria');       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visorias');
    }
}
