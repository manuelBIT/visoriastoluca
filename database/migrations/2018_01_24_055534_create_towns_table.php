<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catmunicipio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('id_estado')->unsigned();
            $table->foreign('id_estado')->references('id')->on('catestado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catmunicipio');
    }
}
