<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('apat_name');
            $table->String('amat_name');
            $table->string('curp_user');
            $table->integer('id_municipio');
            $table->integer('id_estado');
            $table->string('email')->unique();
            $table->string('password');
            $table->date('birthday');
            $table->string('birthplace');
            $table->string('address');
            $table->string('street');
            $table->string('colony');
            $table->string('postal_code');  
            $table->string('city');    
            $table->string('telephone');
            $table->string('mother_name');
            $table->string('father_name');
            $table->string('sent_by');
            $table->string('position');
            $table->string('profile');
            $table->string('weight');
            $table->string('height');
            $table->string('tutor');
            $table->string('have_illness');
            $table->string('scouting_date');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
