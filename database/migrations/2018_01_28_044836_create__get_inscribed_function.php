<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGetInscribedFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            $sql = <<<FinSP
CREATE  FUNCTION Get_inscribed(ocupados INT, descripcion1 VARCHAR(100)) RETURNS INT(11)
BEGIN
DECLARE cuantos INT;
SELECT COUNT(detalle_visorias.descripcion) INTO cuantos FROM detalle_visorias
INNER JOIN visorias ON detalle_visorias.id_visoria = visorias.id
INNER JOIN users ON detalle_visorias.id_usuario = users.id
WHERE detalle_visorias.id_visoria = ocupados
AND descripcion=descripcion1
GROUP BY detalle_visorias.descripcion;
IF cuantos>0 THEN
RETURN cuantos;
ELSE
RETURN 0;
END IF;
END;
$$ LANGUAGE plpgsql;
FinSP;
            DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       $sql = "DROP FUNCTION IF EXISTS Get_inscribed;";
            DB::connection()->getPdo()->exec($sql);
    }
}
