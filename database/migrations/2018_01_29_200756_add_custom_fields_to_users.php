<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('apat_name');
            $table->String('amat_name');
            $table->string('curp_user');
            $table->integer('id_municipio');
            $table->integer('id_estado');
            
            $table->string('birthplace');
            $table->string('address');
            $table->string('street');
            $table->string('colony');
            $table->string('postal_code');  
            $table->string('city');    
            $table->string('telephone');
            $table->string('mother_name');
            $table->string('father_name');
            $table->string('sent_by');
            $table->string('position');
            $table->string('profile');
            $table->string('weight');
            $table->string('height');
            $table->string('tutor');
            $table->string('have_illness');
            $table->string('scouting_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            
            $table->dropColumn('apat_name');
            $table->dropColumn('amat_name');
            $table->dropColumn('curp_user');
            $table->dropColumn('id_municipio');
            $table->dropColumn('id_estado');
            
            $table->dropColumn('birthplace');
            $table->dropColumn('address');
            $table->dropColumn('street');
            $table->dropColumn('colony');
            $table->dropColumn('postal_code');  
            $table->dropColumn('city');    
            $table->dropColumn('telephone');
            $table->dropColumn('mother_name');
            $table->dropColumn('father_name');
            $table->dropColumn('sent_by');
            $table->dropColumn('position');
            $table->dropColumn('profile');
            $table->dropColumn('weight');
            $table->dropColumn('height');
            $table->dropColumn('tutor');
            $table->dropColumn('have_illness');
            $table->dropColumn('scouting_date');
        });
    }
}
